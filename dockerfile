FROM tomcat:latest
RUN rm -rf /usr/local/tomcat/webapps/*
COPY ./target/GFBioRESTfulWSDev.war /usr/local/tomcat/webapps/GFBioRESTfulWSDev.war
COPY ./src/main/resources/watcher.ini /var/opt/ts/watcher.ini
CMD ["catalina.sh","run"]