# Terminology Service V2
## How to run the TS
### Development
For development purposes, it is recommended to clone this repository and to build it locally:
1. change to the project's root directory and execute `mvn clean install`
2. edit the Virtuoso connection URL in `./src/main/resources/watcher.ini`
3. either copy the TS WAR from `./target/GFBioRESTfulWSDev.war`to your local `tomcat` directory or use the provided dockerfile:
	1. execute `docker build -t <container-name>`
	2. run via `docker run -p 8080:8080 -t <container-name>`- this will expose the container's port `8080` to the host system

Open a browser and open http://localhost:8080/GFBioRESTfulWSDev/terminologies 
