package org.gfbio.terminologies.internal;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import org.apache.log4j.Logger;
import org.gfbio.db.VirtGraphSingleton;
import org.gfbio.interfaces.WSInterface;
import org.gfbio.interfaces.WSInterface.SearchModes;
import org.gfbio.interfaces.WSInterface.Services;
import org.gfbio.resultset.CapabilitiesResultSetEntry;
import org.gfbio.resultset.GFBioResultSet;
import org.gfbio.terminologies.environment.StandardVariables;
import org.gfbio.terminologies.exceptions.GFBioBadRequestException;
import org.gfbio.terminologies.exceptions.GFBioInternalException;
import org.gfbio.terminologies.exceptions.GFBioServiceUnavailableException;
import org.gfbio.terminologies.external.ExternalWS;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.rdf.model.Literal;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.shared.JenaException;
import virtuoso.jena.driver.VirtGraph;
import virtuoso.jena.driver.VirtuosoQueryExecution;
import virtuoso.jena.driver.VirtuosoQueryExecutionFactory;

/**
 * Main class for the terminology service Performs different operations over the internal
 * terminologies
 * 
 */

public class GFBioTS {
  private VirtuosoQueryExecution vqe;
  private static GFBioTS instance = null;
  VirtGraph set = VirtGraphSingleton.getInstance().getVirtGraph();
  private static final Logger LOGGER = Logger.getLogger(GFBioTS.class);
  private String metaDataGraph = StandardVariables.getMetadataGraph();
  private String uriPrefix = StandardVariables.getUriprefix();
  // Map to keep track of labels and their corresponding uris
  private ConcurrentHashMap<String, String> uriLabelMap = new ConcurrentHashMap<String, String>();
  private ConcurrentHashMap<String, HashMap<String, String>> terminologies_metadata_map =
      new ConcurrentHashMap<String, HashMap<String, String>>();
  private Services[] availableServices = {Services.term, Services.allterms, Services.synonyms,
      Services.metadata, Services.metrics, Services.search, Services.suggest, Services.broader,
      Services.allbroader, Services.narrower, Services.allnarrower, Services.hierarchy};
  private SearchModes[] searchModes = {SearchModes.exact, SearchModes.included, SearchModes.regex};
  private static final int limitAlltermsService = 10000;

  /**
   * common supported search types
   *
   */
  public enum SearchTypes {
    exact, include, regex, fuzzy
  };

  public GFBioTS() {

  }

  public static GFBioTS getInstance() {
    if (instance == null) {
      instance = new GFBioTS();
    }
    return instance;
  }

  /**
   * Entry point to the output of the suggestSearch service
   * 
   * @param search_string the string to search for
   * @param limit
   * @return ArrayList of result json objects
   */
  public ArrayList<JsonObject> suggestSearch(String search_string, String terminologies_string,
      int limit) {
    String sparql;
    search_string = "\"" + search_string + "\"";
    List<String> terminology_list = new ParameterList(terminologies_string);
    sparql = SPARQLQueryBuilder.getSPARQLSuggestSearch(search_string, terminology_list, limit);
    LOGGER.info("Executing SPARQL query: " + sparql);
    vqe = VirtuosoQueryExecutionFactory.create(sparql, set);
    // LOGGER.info("Executing SPARQL query: " + sparql);
    return createSuggestResult();
  }

  private ArrayList<JsonObject> createSuggestResult() {
    ArrayList<JsonObject> resultList = new ArrayList<JsonObject>();
    ResultSet rs = vqe.execSelect();
    while (rs.hasNext()) {
      JsonObjectBuilder builder = Json.createObjectBuilder();
      QuerySolution result = rs.next();
      if (result.get("label").isLiteral()) {
        builder.add("label", result.get("label").as(Literal.class).getLexicalForm().toString());
      } else {
        builder.add("label", result.get("label").toString());
      }
      builder.add("uri", result.get("uri").toString());
      builder.add("sourceTerminology", result.get("terminology").toString());

      resultList.add(builder.build());
    }
    return resultList;
  }

  /**
   * Create output of the search service
   * 
   * @return ArrayList of result json objects
   */
  private ArrayList<JsonObject> createSearchResult() {
    ResultSet rs = vqe.execSelect();
    if (!rs.hasNext()) {
      return new ArrayList<JsonObject>();
    }
    HashSet<String> displayedUris = new HashSet<String>();
    JsonObjectBuilder builder = Json.createObjectBuilder();
    ArrayList<JsonObject> internal = new ArrayList<JsonObject>();
    while (rs.hasNext()) {
      QuerySolution result = rs.next();
      String uri = result.get("uri").toString();
      // Do not show the same concept multiple times (for example in different
      // languages)
      if (displayedUris.contains(uri)) {
        continue;
      }
      displayedUris.add(uri);
      builder.add("label", result.get("label").as(Literal.class).getString());
      builder.add("uri", uri);
      if (result.contains("url") == true)
        builder.add("url", result.get("url").toString());
      createSearchRest(builder, result.get("graph").toString(), uri);
      builder.add("sourceTerminology", result.get("terminology").toString());
      builder.add("internal", "true");
      internal.add(builder.build());
    }
    LOGGER.info("Internal execution finished, results added = " + internal.size());
    return internal;
  }

  private void createSearchRest(JsonObjectBuilder builder, String graph, String uri) {
    if (!terminologies_metadata_map.containsKey(graph)) {
      insertGraphIntoMetadataMap(graph);
    }
    HashMap<String, String> terminology_metadata = terminologies_metadata_map.get(graph);
    if (!terminology_metadata.isEmpty()) {
      StringBuilder propListBuilder = new StringBuilder();
      String propList = "";
      for (String desc : StandardVariables.getStandarddescriptionattributes()) {
        propListBuilder.append("?defprop = " + desc + " || ");
      }
      if (terminology_metadata.get("definition") != null) {
        propListBuilder.append("?defprop = <" + terminology_metadata.get("definition") + ">");
        propList = propListBuilder.toString();
      } else {
        propList = propListBuilder.substring(0, propListBuilder.length() - 3);
      }
      // Display own definition if present
      String query = "select ?def from <" + graph + "> " + "where {<" + uri + "> "
          + " ?defprop ?def . " + "FILTER(" + propList + ")}";
      LOGGER.info("Executing " + query);
      VirtuosoQueryExecution vqe_ = VirtuosoQueryExecutionFactory.create(query, set);
      ResultSet def_result = vqe_.execSelect();
      if (def_result.hasNext()) {
        QuerySolution definition = def_result.next();

        // check if def can be read as Literal
        if (definition.get("def").isLiteral()) {
          if (!definition.get("def").as(Literal.class).getString().equals("")) {
            builder.add("description", definition.get("def").as(Literal.class).getString());
          }
        }
        vqe_.close();
      } else {
        // Display parent name and/or definition if present
        query = "select ?def ?class from <" + graph + "> " + "where {<" + uri + "> ?p ?classURI ."
            + "OPTIONAL{?classURI  ?defprop ?def . FILTER(" + propList + ")}" + ".?classURI <"
            + terminology_metadata.get("label")
            + "> ?class. FILTER (?p = rdfs:subClassOf || ?p = rdf:type)}";
        LOGGER.info("Executing " + query);
        vqe_ = VirtuosoQueryExecutionFactory.create(query, set);
        def_result = vqe_.execSelect();
        if (def_result.hasNext()) {
          QuerySolution definition = def_result.next();
          if (definition.get("def") != null) {
            if (!definition.get("def").as(Literal.class).getString().equals("")) {
              builder.add("description",
                  "Is a: " + definition.get("class").as(Literal.class).getLexicalForm().toString()
                      + " (" + definition.get("def").as(Literal.class).getString() + ")");
            }
          } else {
            builder.add("description",
                "Is a: " + definition.get("class").as(Literal.class).getLexicalForm().toString());
          }
        }
        vqe_.close();
      }
      if (terminology_metadata.get("synonym") != null) {
        query = "select ?synonym FROM <" + graph + "> " + "WHERE {<" + uri + "> ?p ?synonym."
            + "FILTER(?p = <" + terminology_metadata.get("synonym").trim() + ">"
            + "|| ?p = oboInOwl:hasRelatedSynonym || ?p = skos:altLabel)}";
        LOGGER.info("Executing " + query);
        vqe_ = VirtuosoQueryExecutionFactory.create(query, set);
        ResultSet syn_result = vqe_.execSelect();
        JsonArrayBuilder ja = Json.createArrayBuilder();
        while (syn_result.hasNext()) {
          QuerySolution synonym = syn_result.next();
          ja.add(synonym.get("synonym").as(Literal.class).getString());
        }
        JsonArray synonymArray = ja.build();
        if (!synonymArray.isEmpty()) {
          builder.add("synonyms", synonymArray);
        }
        vqe_.close();
      }
      if (terminology_metadata.get("abbreviation") != null) {
        query =
            "select ?abbreviation FROM <" + graph + "> " + "WHERE {<" + uri + "> ?p ?abbreviation."
                + "FILTER(?p = <" + terminology_metadata.get("abbreviation").trim() + ">" + ")}";
        LOGGER.info("Executing " + query);
        vqe_ = VirtuosoQueryExecutionFactory.create(query, set);
        ResultSet abr_result = vqe_.execSelect();
        JsonArrayBuilder ja = Json.createArrayBuilder();
        while (abr_result.hasNext()) {
          QuerySolution abbre = abr_result.next();
          ja.add(abbre.get("abbreviation").as(Literal.class).getString());
        }
        JsonArray abbreviationArray = ja.build();
        if (!abbreviationArray.isEmpty()) {
          builder.add("abbreviation", abbreviationArray);
        }
        vqe_.close();
      }
      if (terminology_metadata.get("symbol") != null) {
        query = "select ?symbol FROM <" + graph + "> " + "WHERE {<" + uri + "> ?p ?symbol."
            + "FILTER(?p = <" + terminology_metadata.get("symbol").trim() + ">" + ")}";
        LOGGER.info("Executing " + query);
        vqe_ = VirtuosoQueryExecutionFactory.create(query, set);
        ResultSet symbolResult = vqe_.execSelect();
        JsonArrayBuilder ja = Json.createArrayBuilder();
        while (symbolResult.hasNext()) {
          QuerySolution symbol = symbolResult.next();
          ja.add(symbol.get("symbol").as(Literal.class).getString());
        }
        JsonArray symbolArray = ja.build();
        if (!symbolArray.isEmpty()) {
          builder.add("symbol", symbolArray);
        }
      }
      vqe_.close();
    }

  }

  /**
   * Save Metadata about a terminology into a map
   * 
   * @param graph the terminology
   */
  synchronized private void insertGraphIntoMetadataMap(String graph) {
    LOGGER.info("Inserting " + graph);
    HashMap<String, String> metadata_map = new HashMap<String, String>();
    String query = "select ?symbol, ?def, ?synonym, ?abbr, ?label from <" + uriPrefix + "Metadata> "
        + "where { ?o gfbio:graph <" + graph + "> . OPTIONAL{?o gfbio:definition ?def} ."
        + "OPTIONAL{?o gfbio:synonym ?synonym} . OPTIONAL{?o gfbio:abbreviation ?abbr} ."
        + "OPTIONAL{?o gfbio:label ?label} OPTIONAL{?o gfbio:symbol ?symbol}}";
    vqe = VirtuosoQueryExecutionFactory.create(query, set);
    ResultSet res = vqe.execSelect();
    if (res.hasNext()) {
      QuerySolution metadata = res.next();
      if (metadata.get("def") != null) {
        metadata_map.put("definition", metadata.get("def").toString());
      }
      if (metadata.get("synonym") != null) {
        metadata_map.put("synonym", metadata.get("synonym").toString());
      }
      if (metadata.get("abbr") != null) {
        metadata_map.put("abbreviation", metadata.get("abbr").toString());
      }
      if (metadata.get("label") != null) {
        metadata_map.put("label", metadata.get("label").toString());
      }
      if (metadata.get("symbol") != null) {
        metadata_map.put("symbol", metadata.get("symbol").toString());
      }
    }
    vqe.close();
    terminologies_metadata_map.put(graph, metadata_map);
  }

  /**
   * Entry point of term output creation
   * 
   * @param terminology_id terminology acronym
   * @param term_uri term uri
   * @return ArrayList of result json objects
   */
  public ArrayList<JsonObject> term(String terminology_id, String term_uri, String output,
      boolean showQuery) {
    if (output.equals("processed")) {
      return createProcessedTermResult(terminology_id, term_uri);
    } else {
      String sparql;
      try {
        sparql = SPARQLQueryBuilder.getSPARQLTerm(terminology_id, term_uri);
        vqe = VirtuosoQueryExecutionFactory.create(sparql, set);
        if (showQuery)
          LOGGER.info("Executing SPARQL query: " + sparql);
      } catch (JenaException e) {
        if (e.getCause() == null) {
          throw new GFBioBadRequestException();
        } else if (e.getCause().toString().contains("Virtuoso")) {
          throw new GFBioServiceUnavailableException();
        } else {
          throw new GFBioInternalException();
        }
      } catch (Exception e) {
        throw new GFBioInternalException();
      }
      return createTermResult(output, terminology_id, term_uri);
    }
  }

  private ArrayList<JsonObject> createProcessedTermResult(String terminology_id, String term_uri) {
    JsonObjectBuilder builder = Json.createObjectBuilder();
    String sparql;
    sparql = SPARQLQueryBuilder.getSPARQLLabel(terminology_id, term_uri);
    vqe = VirtuosoQueryExecutionFactory.create(sparql, set);
    LOGGER.info("Executing SPARQL query: " + sparql);
    ResultSet rs = vqe.execSelect();
    String label = "";
    if (rs.hasNext()) {
      RDFNode labelNode = rs.next().get("label");
      if (labelNode.isLiteral()) {
        label = labelNode.as(Literal.class).getLexicalForm().toString();
      } else {
        label = labelNode.toString();
      }
    }
    vqe.close();
    builder.add("label", label);
    builder.add("uri", term_uri);
    createSearchRest(builder, uriPrefix + terminology_id, term_uri);
    builder.add("sourceTerminology", terminology_id);
    ArrayList<JsonObject> l = new ArrayList<JsonObject>();
    l.add(builder.build());
    return l;
  }

  /**
   * Create output of the term service
   * 
   * @return ArrayList of result json objects
   */
  private ArrayList<JsonObject> createTermResult(String output, String terminology_id,
      String term_uri) {
    ResultSet rs = vqe.execSelect();
    if (!rs.hasNext()) {
      new ArrayList<JsonObject>();
    }
    JsonObjectBuilder builder = Json.createObjectBuilder();
    // Map to combine values with of the same attribute
    HashMap<String, ArrayList<String>> attributeList = new HashMap<String, ArrayList<String>>();
    // Map to keep track of the uri mapping (terminology specified to internal
    // schema)
    HashMap<String, String> internalUriMap = new HashMap<String, String>();
    while (rs.hasNext()) {
      QuerySolution currentResult = rs.nextSolution();
      // Search for attribute label in internal schema
      if (currentResult.get("attribute") != null
          && uriLabelMap.get(currentResult.get("attribute").toString()) == null) {
        String labelUri = currentResult.get("attribute").toString();
        if (output.equals("combined")) {
          getCombined(terminology_id, labelUri, internalUriMap);
        } else {
          getOriginal(labelUri);
        }
      }
      if (currentResult.get("value") != null) {
        String labeluri;
        if (internalUriMap.get(currentResult.get("attribute").toString()) != null) {
          labeluri = internalUriMap.get(currentResult.get("attribute").toString());
        } else {
          labeluri = currentResult.get("attribute").toString();
        }
        String label = uriLabelMap.get(labeluri);
        ArrayList<String> al = attributeList.get(label);
        if (al == null) {
          al = new ArrayList<String>();
        }
        // Filter language information. Only display if not default language (en)
        if (currentResult.get("value").isLiteral()) {
          Literal v = currentResult.get("value").as(Literal.class);
          if (StandardVariables.getStandardlanguageacronym()
              .contains(v.getLanguage().toLowerCase())) {
            al.add(v.getLexicalForm().toString());
          } else if (v.getLanguage().equals("")) {
            al.add(v.getLexicalForm().toString());
          } else {
            al.add(v.toString());
          }
          attributeList.put(label, al);
        } else {
          al.add(currentResult.get("value").toString());
          attributeList.put(label, al);
        }
      }
    }
    // Combine values with the same attribute to jsonarrays
    for (String l : attributeList.keySet()) {
      JsonArrayBuilder ab = Json.createArrayBuilder();
      ArrayList<String> al = attributeList.get(l);
      if (al.size() > 1) {
        for (String v : attributeList.get(l)) {
          ab.add(v);
        }
        builder.add(l, ab.build());
      } else {
        builder.add(l, al.get(0));
      }
      if (output.equals("combined")) {
        builder.add("sourceTerminology", terminology_id);
        builder.add("uri", term_uri);
      }
    }
    // Create Context for JSONLD
    Iterator<Map.Entry<String, String>> it = uriLabelMap.entrySet().iterator();
    JsonObjectBuilder ob = Json.createObjectBuilder();
    while (it.hasNext()) {
      Map.Entry<String, String> pair = (Map.Entry<String, String>) it.next();
      ob.add(pair.getValue(), pair.getKey());
      it.remove();
    }
    ArrayList<JsonObject> resultList = new ArrayList<JsonObject>();
    resultList.add(builder.build());
    resultList.add(Json.createObjectBuilder().add("context", ob.build()).build());
    return resultList;
  }

  private void getOriginal(String labelUri) {
    String query = "select * where {graph ?graph{<" + labelUri + "> ?p ?o} . "
        + "FILTER (?p = <http://www.w3.org/2000/01/rdf-schema#label>)}";
    vqe = VirtuosoQueryExecutionFactory.create(query.toString(),
        VirtGraphSingleton.getInstance().getVirtGraph());
    ResultSet labelResult = vqe.execSelect();
    String label = labelUri;
    if (labelResult.hasNext()) {
      label = labelResult.nextSolution().get("o").as(Literal.class).getLexicalForm().toString();
    }
    uriLabelMap.put(labelUri, label);
  }

  private void getCombined(String terminology_id, String labelUri,
      HashMap<String, String> internalUriMap) {
    String query = "select distinct ?tsapiAttribute ?label from " + "<"
        + StandardVariables.getUriprefix() + metaDataGraph + "> " + "from <"
        + StandardVariables.getMetadaschema() + "> from <" + StandardVariables.getTSschema()
        + "> where {" + "?graph gfbio:graph <" + StandardVariables.getUriprefix() + terminology_id
        + "> . ?graph ?attribute " + "<" + labelUri
        + "> . ?attribute rdfs:subPropertyOf ?tsapiAttribute . ?tsapiAttribute rdfs:label ?label }";
    vqe = VirtuosoQueryExecutionFactory.create(query,
        VirtGraphSingleton.getInstance().getVirtGraph());
    LOGGER.info("Executing SPARQL query: " + query);
    ResultSet labelResult = vqe.execSelect();
    String label = labelUri;
    if (labelResult.hasNext()) {
      QuerySolution labelSol = labelResult.next();
      internalUriMap.put(labelUri, labelSol.get("tsapiAttribute").toString());
      label = labelSol.get("label").as(Literal.class).getLexicalForm().toString();
      labelUri = labelSol.get("tsapiAttribute").toString();
      uriLabelMap.put(labelUri, label);
    } else {
      getOriginal(labelUri);
    }
    vqe.close();
    uriLabelMap.put("http://terminologies.gfbio.org/terms/ts-schema/sourceTerminology",
        "sourceTerminology");
    uriLabelMap.put("http://terminologies.gfbio.org/terms/ts-schema/uri", "uri");
  }

  /**
   * Create output of the broader service
   * 
   * @param terminology_id terminology acronym
   * @param term_uri term uri
   * @return ArrayList of result json objects
   */
  public ArrayList<JsonObject> broader(String terminology_id, String term_uri) {
    String sparql;
    ArrayList<JsonObject> termInfos = new ArrayList<JsonObject>();
    try {
      sparql = SPARQLQueryBuilder.getSPARQLBroader(terminology_id, term_uri);
      vqe = VirtuosoQueryExecutionFactory.create(sparql, set);
      LOGGER.info("Executing SPARQL query: " + sparql);
      ResultSet rs = vqe.execSelect();
      while (rs.hasNext()) {
        QuerySolution current_result = rs.next();
        JsonObjectBuilder builder = Json.createObjectBuilder();
        builder.add("uri", current_result.get("broaderuri").toString());
        builder.add("label",
            current_result.get("broaderlabel").as(Literal.class).getLexicalForm().toString());
        termInfos.add(builder.build());
      }
    } catch (JenaException e) {
      if (e.getCause() == null) {
        throw new GFBioBadRequestException();
      } else if (e.getCause().toString().contains("Virtuoso")) {
        throw new GFBioServiceUnavailableException();
      } else {
        throw new GFBioInternalException();
      }
    } catch (Exception e) {
      throw new GFBioInternalException();
    }
    vqe.close();
    return termInfos;
  }

  /**
   * Create output of narrower service
   * 
   * @param terminology_id terminology acronym
   * @param term_uri term uri
   * @return ArrayList of result json objects
   */
  public ArrayList<JsonObject> narrower(String terminology_id, String term_uri) {
    String sparql;
    ArrayList<JsonObject> termInfos = new ArrayList<JsonObject>();
    try {
      sparql = SPARQLQueryBuilder.getSPARQLNarrower(terminology_id, term_uri);
      vqe = VirtuosoQueryExecutionFactory.create(sparql, set);
      LOGGER.info("Executing SPARQL query: " + sparql);
      ResultSet rs = vqe.execSelect();
      while (rs.hasNext()) {
        QuerySolution current_result = rs.next();
        JsonObjectBuilder builder = Json.createObjectBuilder();
        builder.add("uri", current_result.get("narroweruri").toString());
        builder.add("label",
            current_result.get("narrowerlabel").as(Literal.class).getLexicalForm().toString());
        termInfos.add(builder.build());
      }
    } catch (JenaException e) {
      if (e.getCause() == null) {
        throw new GFBioBadRequestException();
      } else if (e.getCause().toString().contains("Virtuoso")) {
        throw new GFBioServiceUnavailableException();
      } else {
        throw new GFBioInternalException();
      }
    } catch (Exception e) {
      throw new GFBioInternalException();
    }
    vqe.close();
    return termInfos;
  }

  /**
   * Create output of allbroader service
   * 
   * @param terminology_id terminology acronym
   * @param term_uri term uri
   * @return ArrayList of result json objects
   */
  public ArrayList<JsonObject> allBroader(String terminology_id, String term_uri) {
    String sparql;
    ArrayList<JsonObject> termInfos = new ArrayList<JsonObject>();
    try {
      sparql = SPARQLQueryBuilder.getSPARQLAllBroader(terminology_id, term_uri);
      vqe = VirtuosoQueryExecutionFactory.create(sparql, set);
      LOGGER.info("Executing SPARQL query: " + sparql);
      ResultSet rs = vqe.execSelect();
      while (rs.hasNext()) {
        QuerySolution current_result = rs.next();
        JsonObjectBuilder builder = Json.createObjectBuilder();
        builder.add("uri", current_result.get("broaderuri").toString());
        builder.add("label",
            current_result.get("broaderlabel").as(Literal.class).getLexicalForm().toString());
        termInfos.add(builder.build());
      }
    } catch (JenaException e) {
      if (e.getCause() == null) {
        throw new GFBioBadRequestException();
      } else if (e.getCause().toString().contains("Virtuoso")) {
        throw new GFBioServiceUnavailableException();
      } else {
        throw new GFBioInternalException();
      }
    } catch (Exception e) {
      throw new GFBioInternalException();
    }
    vqe.close();
    return termInfos;
  }

  /**
   * Create output of the hierarchy service
   * 
   * @param terminology_id terminology acronym
   * @param term_uri term uri
   * @return ArrayList of result json objects
   */
  public ArrayList<JsonObject> hierarchy(String terminology_id, String term_uri) {
    String sparql;
    ArrayList<JsonObject> termInfos = new ArrayList<JsonObject>();
    ArrayDeque<Pair<String, String>> queue = new ArrayDeque<Pair<String, String>>();
    ArrayList<String> printedConcepts = new ArrayList<String>();

    // FIXME result of this code is not used?!
    // sparql = SPARQLQueryBuilder.getSPARQLType(terminology_id, term_uri);
    // vqe = VirtuosoQueryExecutionFactory.create(sparql, set);
    // LOGGER.info("Executing SPARQL query: " + sparql);
    // ResultSet rs = vqe.execSelect();
    sparql = SPARQLQueryBuilder.getSPARQLLabel(terminology_id, term_uri);
    vqe = VirtuosoQueryExecutionFactory.create(sparql, set);
    LOGGER.info("Executing SPARQL query: " + sparql);
    ResultSet rs = vqe.execSelect();
    String label = "";
    while (rs.hasNext()) {
      label = rs.next().get("label").as(Literal.class).getLexicalForm().toString();
    }
    queue.addLast(new Pair<String, String>(label, term_uri));
    while (!queue.isEmpty()) {
      Pair<String, String> entry = queue.removeFirst();
      if (!entry.getLeft().equals("")) {
        try {
          term_uri = entry.getRight();
          sparql = SPARQLQueryBuilder.getSPARQLBroader(terminology_id, term_uri);
          vqe = VirtuosoQueryExecutionFactory.create(sparql, set);
          LOGGER.info("Executing SPARQL query: " + sparql);
          rs = vqe.execSelect();
          JsonObjectBuilder builder = Json.createObjectBuilder();
          JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
          builder.add("label", entry.getLeft());
          builder.add("uri", term_uri);
          while (rs.hasNext()) {
            QuerySolution current_result = rs.next();
            String result_uri = current_result.get("broaderuri").toString();
            Pair<String, String> a = new Pair<String, String>(
                current_result.get("broaderlabel").as(Literal.class).getLexicalForm().toString(),
                result_uri);
            if (!printedConcepts.contains(result_uri) && !queue.contains(a)) {
              queue.addLast(a);
            }
            arrayBuilder.add(result_uri);
          }
          builder.add("hierarchy", arrayBuilder.build());
          printedConcepts.add(term_uri);
          termInfos.add(builder.build());
        } catch (JenaException e) {
          if (e.getCause() == null) {
            throw new GFBioBadRequestException();
          } else if (e.getCause().toString().contains("Virtuoso")) {
            throw new GFBioServiceUnavailableException();
          } else {
            throw new GFBioInternalException();
          }
        } catch (Exception e) {
          throw new GFBioInternalException();
        }
      }
    }
    vqe.close();
    return termInfos;
  }

  /**
   * Create output of the allnarrower service
   * 
   * @param terminology_id terminology acronym
   * @param term_uri term uri
   * @return ArrayList of result json objects
   */
  public ArrayList<JsonObject> allNarrower(String terminology_id, String term_uri) {
    String sparql;
    ArrayList<JsonObject> termInfos = new ArrayList<JsonObject>();
    try {
      sparql = SPARQLQueryBuilder.getSPARQLAllNarrower(terminology_id, term_uri);
      vqe = VirtuosoQueryExecutionFactory.create(sparql, set);
      LOGGER.info("Executing SPARQL query: " + sparql);
      ResultSet rs = vqe.execSelect();
      while (rs.hasNext()) {
        QuerySolution current_result = rs.next();
        JsonObjectBuilder builder = Json.createObjectBuilder();
        builder.add("uri", current_result.get("narroweruri").toString());
        builder.add("label",
            current_result.get("narrowerlabel").as(Literal.class).getLexicalForm().toString());
        termInfos.add(builder.build());
      }
    } catch (JenaException e) {
      if (e.getCause() == null) {
        throw new GFBioBadRequestException();
      } else if (e.getCause().toString().contains("Virtuoso")) {
        throw new GFBioServiceUnavailableException();
      } else {
        throw new GFBioInternalException();
      }
    } catch (Exception e) {
      throw new GFBioInternalException();
    }
    vqe.close();
    return termInfos;
  }

  /**
   * Create output of the allterms service
   * 
   * @param terminology_id terminology acronym
   * @return ArrayList of result json objects
   */
  public ArrayList<JsonObject> allTerms(String terminology_id) {
    ArrayList<JsonObject> termInfos = new ArrayList<JsonObject>();
    String sparql = SPARQLQueryBuilder.getSPARQLClassesNumber(terminology_id);
    vqe = VirtuosoQueryExecutionFactory.create(sparql, set);
    ResultSet res = vqe.execSelect();
    int classesCount = Integer.parseInt(res.next().get("number").toString());
    // FIXME re-aktivieren
    if (classesCount > limitAlltermsService) {
      JsonObjectBuilder builder = Json.createObjectBuilder();
      builder.add("warning", "The requested terminology is too big to list all terms.");
      termInfos.add(builder.build());
      return termInfos;
    }
    vqe.close();
    try {
      sparql = SPARQLQueryBuilder.getSPARQLAllTerms(terminology_id);
      vqe = VirtuosoQueryExecutionFactory.create(sparql, set);
      LOGGER.info("Executing SPARQL query: " + sparql);
      ResultSet rs = vqe.execSelect();
      while (rs.hasNext()) {
        QuerySolution current_result = rs.next();
        JsonObjectBuilder builder = Json.createObjectBuilder();
        builder.add("uri", current_result.get("uri").toString());
        builder.add("label",
            current_result.get("label").as(Literal.class).getLexicalForm().toString());
        termInfos.add(builder.build());
      }
    } catch (JenaException e) {
      if (e.getCause() == null) {
        throw new GFBioBadRequestException();
      } else if (e.getCause().toString().contains("Virtuoso")) {
        throw new GFBioServiceUnavailableException();
      } else {
        throw new GFBioInternalException();
      }
    } catch (Exception e) {
      throw new GFBioInternalException();
    }
    vqe.close();
    return termInfos;
  }

  /**
   * Create output of the terminologies service
   * 
   * @return ArrayList of result json objects
   */
  public ArrayList<JsonObject> terminologies() {
    String sparql;
    ArrayList<JsonObject> termInfos = new ArrayList<JsonObject>();
    // try{
    String omvURI = StandardVariables.getOmvuri();
    String tsURI = StandardVariables.getMetadaschema();
    sparql = SPARQLQueryBuilder.getSPARQLTerminologies();
    vqe = VirtuosoQueryExecutionFactory.create(sparql, set);
    ResultSet rs = vqe.execSelect();
    // Internal terminology information
    while (rs.hasNext()) {
      QuerySolution current_result = rs.next();
      JsonObjectBuilder builder = Json.createObjectBuilder();
      builder.add("name", current_result.get("name").as(Literal.class).getLexicalForm().toString());
      builder.add("acronym",
          current_result.get("acronym").as(Literal.class).getLexicalForm().toString());
      builder.add("description",
          current_result.get("description").as(Literal.class).getLexicalForm().toString());
      builder.add("uri", current_result.get("uri").toString());
      builder.add("storage", "internal");
      termInfos.add(builder.build());
    }
    // External Webservice information
    ExternalWS externalWs = new ExternalWS();
    HashMap<String, WSInterface> wsList = externalWs.getWsList();
    Iterator<Entry<String, WSInterface>> it = wsList.entrySet().iterator();
    while (it.hasNext()) {
      Map.Entry<String, WSInterface> pair = (Map.Entry<String, WSInterface>) it.next();
      JsonObjectBuilder builder = Json.createObjectBuilder();
      builder.add("name", pair.getValue().getName());
      builder.add("acronym", pair.getValue().getAcronym());
      builder.add("description", pair.getValue().getDescription());
      builder.add("uri", pair.getValue().getURI());
      builder.add("storage", "external");
      termInfos.add(builder.build());
      it.remove();
    }
    JsonObjectBuilder contextbuilder = Json.createObjectBuilder();
    contextbuilder.add("name", omvURI + "name");
    contextbuilder.add("acronym", omvURI + "acronym");
    contextbuilder.add("description", omvURI + "description");
    contextbuilder.add("uri", omvURI + "URI");
    contextbuilder.add("storage", tsURI + "storage");
    termInfos.add(Json.createObjectBuilder().add("context", contextbuilder.build()).build());
    // } catch (JenaException e) {
    // if (e.getCause() == null) {
    // throw new GFBioBadRequestException();
    // } else if (e.getCause().toString().contains("Virtuoso")) {
    // throw new GFBioServiceUnavailableException();
    // } else {
    // throw new GFBioInternalException();
    // }
    // } catch (Exception e) {
    // throw new GFBioInternalException();
    // }
    vqe.close();
    return termInfos;
  }

  /**
   * Returns a list of metadata for archived terminology versions, if available
   * 
   * @param terminology_id terminology acronym
   * @return ArrayList of result json objects
   */
  public JsonObject archive(String terminologyId, int archivedId) {
    JsonObjectBuilder result_builder = Json.createObjectBuilder();
    // JsonArray archive;
    JsonArrayBuilder arrBuilder = Json.createArrayBuilder();
    // String sparql = "SELECT * FROM <" + uriPrefix + "Metadata> WHERE {<" + uriPrefix
    // + terminologyId.hashCode() + "> ?p ?o. FILTER(?p = gfbio:lastArchiveID) }";
    vqe = VirtuosoQueryExecutionFactory
        .create(SPARQLQueryBuilder.getSPARQLTerminologyArchive(terminologyId), set);
    LOGGER.info(
        "Executing SPARQL query: " + SPARQLQueryBuilder.getSPARQLTerminologyArchive(terminologyId));

    int lastArchiveID = 0;
    ResultSet rs = vqe.execSelect();
    while (rs.hasNext()) {
      QuerySolution result = rs.nextSolution();
      lastArchiveID = Integer.valueOf(result.get("o").asLiteral().getInt());
    }
    if (lastArchiveID > 0) {
      for (int i = lastArchiveID; i > 0; i--) {

        // JsonObjectBuilder builder = Json.createObjectBuilder();
        String sparql = "SELECT * FROM <" + uriPrefix + "Metadata> WHERE {<" + uriPrefix
            + terminologyId.hashCode() + "/" + i + "> ?p ?o.}";
        LOGGER.info(sparql);
        vqe = VirtuosoQueryExecutionFactory.create(sparql, set);
        rs = vqe.execSelect();
        JsonObjectBuilder builder = Json.createObjectBuilder();
        while (rs.hasNext()) {
          QuerySolution result = rs.nextSolution();
          String lang = result.get("p").asResource().getLocalName();
          String graph = result.get("o").toString();

          if (!lang.equalsIgnoreCase("type"))
            builder.add(lang, graph.trim());
        }
        if (archivedId == 0)
          arrBuilder.add(builder.build());
        else if (archivedId == i) {
          vqe.close();
          return builder.build();
        }
      }
      vqe.close();
      result_builder.add("results", arrBuilder.build());
    }
    return result_builder.build();
  }

  public JsonObject latestTerminologyVersion(String terminologyId) {
    String sparql;
    JsonObjectBuilder terminologyMetadata = Json.createObjectBuilder();
    // JsonArrayBuilder arrBuilder = Json.createArrayBuilder();
    try {
      sparql = SPARQLQueryBuilder.getSPARQLTerminologyInfo(terminologyId);
      vqe = VirtuosoQueryExecutionFactory.create(sparql, set);
      LOGGER.info("Executing SPARQL query: " + sparql);
      ResultSet rs = vqe.execSelect();
      // JsonObjectBuilder builder = Json.createObjectBuilder();
      JsonObjectBuilder contextbuilder = Json.createObjectBuilder();
      // JsonObjectBuilder metadata = Json.createObjectBuilder();
      // (1) collect latest terminology version metadata
      while (rs.hasNext()) {

        QuerySolution currentResult = rs.next();

        String label;
        if (currentResult.get("label") != null) {
          if (currentResult.get("label").isLiteral()) {
            label = currentResult.get("label").as(Literal.class).getLexicalForm().toString();
          } else {
            label = currentResult.get("label").toString();
          }
        } else {
          label = currentResult.get("attribute").toString();
        }
        String value;
        if (currentResult.get("value").isLiteral()) {
          value = currentResult.get("value").as(Literal.class).getLexicalForm().toString();
        } else {
          value = currentResult.get("value").toString();
        }
        // FIXME FB 06/01 DlExpressivity is not calculated correctly and shall be omitted until it
        // is fixed
        if (!label.equalsIgnoreCase("hasDlExpressivity")) {
          terminologyMetadata.add(label, value);
          contextbuilder.add(label, currentResult.get("attribute").toString());
        }
      }
      terminologyMetadata.add("storage", "internal");
      // FIXME
      // terminologyMetadata.add("context", contextbuilder.build());
      // String tsURI = StandardVariables.getMetadaschema();
      // terminologyMetadata.add("storage", tsURI + "storage");
      // arrBuilder.add(metadata.build());

    } catch (JenaException e) {
      if (e.getCause() == null) {
        throw new GFBioBadRequestException();
      } else if (e.getCause().toString().contains("Virtuoso")) {
        throw new GFBioServiceUnavailableException();
      } else {
        throw new GFBioInternalException();
      }
    } catch (Exception e) {
      throw new GFBioInternalException();
    }
    vqe.close();
    // terminologyMetadata.add("latest", arrBuilder.build());
    return terminologyMetadata.build();
  }

  public JsonObject changelog(String terminologyId, String serviceBaseUrl, int page, int pagesize) {

    JsonObjectBuilder allTerms = Json.createObjectBuilder();
    JsonArray modified;
    ResultSet rs;
    JsonArrayBuilder builder = Json.createArrayBuilder();
    int modcount = 0, offset = 0;
    if (page > 1)
      offset = pagesize * (page - 1);

    LOGGER.info("collecting changed terms");

    String countQuery = "SELECT COUNT(*) AS ?cnt FROM <" + uriPrefix + terminologyId
        + "MOD> WHERE { ?s ?p ?o. ?o <http://terminologies.gfbio.org/terms/ontology#change> ?change . }";
    vqe = VirtuosoQueryExecutionFactory.create(countQuery, set);
    rs = vqe.execSelect();
    while (rs.hasNext()) {
      QuerySolution qs = rs.nextSolution();
      modcount = qs.get("cnt").asLiteral().getInt();
    }


    String sparql = "SELECT * FROM <" + uriPrefix + terminologyId
        + "MOD> WHERE { ?s ?p ?o. ?o <http://terminologies.gfbio.org/terms/ontology#change> ?change . } ORDER BY ASC(?change) LIMIT "
        + pagesize + " OFFSET " + offset;
    LOGGER.info(sparql);

    vqe = VirtuosoQueryExecutionFactory.create(sparql, set);
    rs = vqe.execSelect();

    while (rs.hasNext()) {
      String URI = "";

      QuerySolution qs = rs.nextSolution();
      // extract URI and status of term
      URI = qs.get("o").asResource().toString();

      if (qs.get("change").asLiteral().toString().equalsIgnoreCase("modified")
          || qs.get("change").asLiteral().toString().equalsIgnoreCase("added")) {
        // get complete term info for URI
        ArrayList<JsonObject> res = term(terminologyId, URI, "", false);
        if (!res.get(0).isEmpty()) {
          JsonObjectBuilder termBuilder = Json.createObjectBuilder();
          termBuilder.add("uri", URI);
          for (String key : res.get(0).keySet())
            termBuilder.add(key, res.get(0).get(key));
          builder.add(termBuilder.build());
        }
      } else {
        // for removed terms we only display the URI
        JsonObjectBuilder termBuilder = Json.createObjectBuilder();
        termBuilder.add("uri", URI);
        builder.add(termBuilder.build());
      }
      // modcount++;
    }
    modified = builder.build();
    vqe.close();

    // LOGGER.info("collecting added terms");
    // builder = Json.createArrayBuilder();
    // sparql = "SELECT * FROM <" + uriPrefix + terminologyId
    // + "MOD> WHERE { ?s ?p ?o. ?o <http://terminologies.gfbio.org/terms/ontology#status> ?status .
    // FILTER(?status=\"added\")} LIMIT "
    // + pagesize + " OFFSET " + offset;
    // vqe = VirtuosoQueryExecutionFactory.create(sparql, set);
    //
    // rs = vqe.execSelect();
    // while (rs.hasNext()) {
    // String URI = "";
    //
    // QuerySolution qs = rs.nextSolution();
    // // extract URI and status of term
    // URI = qs.get("o").asResource().toString();
    //
    // // get complete term info for URI
    // ArrayList<JsonObject> res = term(terminologyId, URI, "", false);
    // if (!res.get(0).isEmpty()) {
    // JsonObjectBuilder termBuilder = Json.createObjectBuilder();
    // termBuilder.add("uri", URI);
    // for (String key : res.get(0).keySet())
    // termBuilder.add(key, res.get(0).get(key));
    // builder.add(termBuilder.build());
    // }
    // addcount++;
    // }
    // added = builder.build();
    // vqe.close();
    //
    // LOGGER.info("collecting deleted terms");
    // builder = Json.createArrayBuilder();
    // sparql = "SELECT * FROM <" + uriPrefix + terminologyId
    // + "MOD> WHERE { ?s ?p ?o. ?o <http://terminologies.gfbio.org/terms/ontology#status> ?status .
    // FILTER(?status=\"deleted\")} LIMIT "
    // + pagesize + " OFFSET " + offset;
    // vqe = VirtuosoQueryExecutionFactory.create(sparql, set);
    //
    // rs = vqe.execSelect();
    // while (rs.hasNext()) {
    // String URI = "";
    //
    // QuerySolution qs = rs.nextSolution();
    // // extract URI and status of term
    // URI = qs.get("o").asResource().toString();
    // JsonObjectBuilder termBuilder = Json.createObjectBuilder();
    // termBuilder.add("uri", URI);
    // builder.add(termBuilder.build());
    // delcount++;
    // }
    // deleted = builder.build();

    // String versionInfo = "N.A.";
    // sparql = SPARQLQueryBuilder.getSPARQLMetadata(terminologyId);
    // vqe = VirtuosoQueryExecutionFactory.create(sparql, set);
    // LOGGER.info("Executing SPARQL query: " + sparql);
    // rs = vqe.execSelect();
    // while (rs.hasNext()) {
    // QuerySolution currentResult = rs.next();
    // if (currentResult.get("attribute").asResource().toString().contains("versionInfo"))
    // versionInfo = currentResult.get("value").asLiteral().getValue().toString();
    // }
    // vqe.close();

    if (modified.size() > 0) {

      JsonObjectBuilder links = Json.createObjectBuilder();
      // allTerms.add("versionInfo", versionInfo);
      allTerms.add("count", modcount);
      allTerms.add("page", page);
      allTerms.add("pageCount", (modcount / pagesize) + 1);
      if (page - 1 > 0) {
        allTerms.add("prevPage", page - 1);
        links.add("prevPage", serviceBaseUrl + "?page=" + (page - 1) + "&pagesize=" + pagesize);
      } else {
        allTerms.add("prevPage", JsonObject.NULL);
        links.add("prevPage", JsonObject.NULL);
      }
      if (page >= (modcount / pagesize) + 1) {
        links.add("nextPage", JsonObject.NULL);
        allTerms.add("nextPage", JsonObject.NULL);
      } else {
        allTerms.add("nextPage", page + 1);
        links.add("nextPage", serviceBaseUrl + "?page=" + (page + 1) + "&pagesize=" + pagesize);
      }
      allTerms.add("links", links.build());

      allTerms.add("results", modified);
    }
    // allTerms.add("added_count", addcount);
    // allTerms.add("added", added);
    // allTerms.add("deleted_count", delcount);
    // allTerms.add("deleted", deleted);

    return allTerms.build();
  }

  /**
   * Create output of the terminolgyinfos service
   * 
   * @param terminology_id terminology acronym
   * @return ArrayList of result json objects
   */
  public ArrayList<JsonObject> metadata(String terminology_id) {

    JsonObjectBuilder builder = Json.createObjectBuilder();
    // Map to combine values with of the same attribute
    HashMap<String, ArrayList<String>> attributeList = new HashMap<String, ArrayList<String>>();

    String sparql;
    sparql = SPARQLQueryBuilder.getSPARQLMetadata(terminology_id);
    vqe = VirtuosoQueryExecutionFactory.create(sparql, set);
    LOGGER.info("Executing SPARQL query: " + sparql);
    ResultSet rs = vqe.execSelect();
    while (rs.hasNext()) {
      QuerySolution currentResult = rs.next();
      if (currentResult.get("attribute") != null
          && uriLabelMap.get(currentResult.get("attribute").toString()) == null) {
        String labelUri = currentResult.get("attribute").toString();
        String query = "select ?label where {graph ?graph{<" + labelUri + "> ?p ?label} . "
            + "FILTER (?p = <http://www.w3.org/2000/01/rdf-schema#label>)}";
        vqe = VirtuosoQueryExecutionFactory.create(query, set);
        ResultSet labelResult = vqe.execSelect();
        String label = labelUri;
        if (labelResult.hasNext()) {
          QuerySolution labelSol = labelResult.next();
          label = labelSol.get("label").as(Literal.class).getLexicalForm().toString();
        }
        uriLabelMap.put(labelUri, label);
      }
      if (currentResult.get("value") != null) {
        String labeluri = currentResult.get("attribute").toString();
        String label = uriLabelMap.get(labeluri);
        ArrayList<String> al = attributeList.get(label);
        if (al == null) {
          al = new ArrayList<String>();
        }
        if (currentResult.get("value").isLiteral()) {
          Literal v = currentResult.get("value").as(Literal.class);
          al.add(v.getLexicalForm().toString());
          attributeList.put(label, al);
        } else {
          al.add(currentResult.get("value").toString());
          attributeList.put(label, al);
        }
      }
    }
    // Combine same attributes
    for (String l : attributeList.keySet()) {
      JsonArrayBuilder ab = Json.createArrayBuilder();
      ArrayList<String> al = attributeList.get(l);
      if (al.size() > 1) {
        for (String v : attributeList.get(l)) {
          ab.add(v);
        }
        builder.add(l, ab.build());
      } else {
        builder.add(l, al.get(0));
      }
    }
    // Create Context for JSONLD
    Iterator<Map.Entry<String, String>> it = uriLabelMap.entrySet().iterator();
    JsonObjectBuilder ob = Json.createObjectBuilder();
    while (it.hasNext()) {
      Map.Entry<String, String> pair = (Map.Entry<String, String>) it.next();
      ob.add(pair.getValue(), pair.getKey());
      it.remove();
    }
    vqe.close();
    ArrayList<JsonObject> resultList = new ArrayList<JsonObject>();
    resultList.add(builder.build());
    resultList.add(Json.createObjectBuilder().add("context", ob.build()).build());
    return resultList;
  }

  /**
   * Create output of the metrics service
   * 
   * @param terminology_id terminology acronym
   * @return ArrayList of result json objects
   */
  public ArrayList<JsonObject> metrics(String terminology_id) {
    String sparql;
    ArrayList<JsonObject> terminologyMetrics = new ArrayList<JsonObject>();
    try {
      sparql = SPARQLQueryBuilder.getSPARQLMetrics(terminology_id);
      vqe = VirtuosoQueryExecutionFactory.create(sparql, set);
      LOGGER.info("Executing SPARQL query: " + sparql);
      ResultSet rs = vqe.execSelect();
      JsonObjectBuilder builder = Json.createObjectBuilder();
      JsonObjectBuilder contextbuilder = Json.createObjectBuilder();
      while (rs.hasNext()) {
        QuerySolution currentResult = rs.next();

        String label;
        if (currentResult.get("label") != null) {
          if (currentResult.get("label").isLiteral()) {
            label = currentResult.get("label").as(Literal.class).getLexicalForm().toString();
          } else {
            label = currentResult.get("label").toString();
          }
        } else {
          label = currentResult.get("attribute").toString();
        }
        String value;
        if (currentResult.get("value").isLiteral()) {
          value = currentResult.get("value").as(Literal.class).getLexicalForm().toString();
        } else {
          value = currentResult.get("value").toString();
        }
        builder.add(label, value);
        contextbuilder.add(label, currentResult.get("attribute").toString());
      }
      terminologyMetrics.add(builder.build());
      terminologyMetrics
          .add(Json.createObjectBuilder().add("context", contextbuilder.build()).build());
    } catch (JenaException e) {
      if (e.getCause() == null) {
        throw new GFBioBadRequestException();
      } else if (e.getCause().toString().contains("Virtuoso")) {
        throw new GFBioServiceUnavailableException();
      } else {
        throw new GFBioInternalException();
      }
    } catch (Exception e) {
      throw new GFBioInternalException();
    }
    vqe.close();
    return terminologyMetrics;
  }

  /**
   * Create output of the metadata service
   * 
   * @param terminology_id terminology acronym
   * @return ArrayList of result json objects
   */
  public ArrayList<JsonObject> terminologyInfos(String terminology_id) {
    String sparql;
    ArrayList<JsonObject> terminologyMetadata = new ArrayList<JsonObject>();
    try {
      sparql = SPARQLQueryBuilder.getSPARQLTerminologyInfo(terminology_id);
      vqe = VirtuosoQueryExecutionFactory.create(sparql, set);
      LOGGER.info("Executing SPARQL query: " + sparql);
      ResultSet rs = vqe.execSelect();
      JsonObjectBuilder builder = Json.createObjectBuilder();
      JsonObjectBuilder contextbuilder = Json.createObjectBuilder();
      // (1) collect latest terminology version metadata
      while (rs.hasNext()) {
        QuerySolution currentResult = rs.next();

        String label;
        if (currentResult.get("label") != null) {
          if (currentResult.get("label").isLiteral()) {
            label = currentResult.get("label").as(Literal.class).getLexicalForm().toString();
          } else {
            label = currentResult.get("label").toString();
          }
        } else {
          label = currentResult.get("attribute").toString();
        }
        String value;
        if (currentResult.get("value").isLiteral()) {
          value = currentResult.get("value").as(Literal.class).getLexicalForm().toString();
        } else {
          value = currentResult.get("value").toString();
        }
        // FIXME FB 06/01 DlExpressivity is not calculated correctly and shall be omitted until it
        // is fixed
        if (!label.equalsIgnoreCase("hasDlExpressivity")) {
          builder.add(label, value);
          contextbuilder.add(label, currentResult.get("attribute").toString());
        }
      }
      builder.add("storage", "internal");
      String tsURI = StandardVariables.getMetadaschema();
      contextbuilder.add("storage", tsURI + "storage");
      terminologyMetadata.add(builder.build());
      terminologyMetadata
          .add(Json.createObjectBuilder().add("context", contextbuilder.build()).build());

      // (2) collect archived versions metadata
      // terminologyMetadata.add(archive(terminology_id, 0));

    } catch (JenaException e) {
      if (e.getCause() == null) {
        throw new GFBioBadRequestException();
      } else if (e.getCause().toString().contains("Virtuoso")) {
        throw new GFBioServiceUnavailableException();
      } else {
        throw new GFBioInternalException();
      }
    } catch (Exception e) {
      throw new GFBioInternalException();
    }
    vqe.close();
    return terminologyMetadata;
  }

  /**
   * Create output of the synonym service
   * 
   * @param terminology_id terminology acronym
   * @param term_uri term uri
   * @return ArrayList of result json objects
   */
  public ArrayList<JsonObject> synonym(String terminology_id, String term_uri) {
    String sparql;
    ArrayList<JsonObject> termInfos = new ArrayList<JsonObject>();
    try {
      sparql = SPARQLQueryBuilder.getSPARQLSynonym(terminology_id, term_uri);
      vqe = VirtuosoQueryExecutionFactory.create(sparql, set);
      LOGGER.info("Executing SPARQL query: " + sparql);
      ResultSet rs = vqe.execSelect();
      JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
      JsonObjectBuilder builder = Json.createObjectBuilder();
      if (rs.hasNext()) {
        while (rs.hasNext()) {
          QuerySolution current_result = rs.next();
          arrayBuilder
              .add(current_result.get("synonym").as(Literal.class).getLexicalForm().toString());
        }

        builder.add("synonyms", arrayBuilder.build());
        termInfos.add(builder.build());
      }
    } catch (JenaException e) {
      if (e.getCause() == null) {
        throw new GFBioBadRequestException();
      } else if (e.getCause().toString().contains("Virtuoso")) {
        throw new GFBioServiceUnavailableException();
      } else {
        throw new GFBioInternalException();
      }
    } catch (Exception e) {
      throw new GFBioInternalException();
    }
    vqe.close();
    return termInfos;
  }

  private class Pair<L, R> {

    private final L left;
    private final R right;

    public Pair(L left, R right) {
      this.left = left;
      this.right = right;
    }

    public L getLeft() {
      return left;
    }

    public R getRight() {
      return right;
    }

    @Override
    public int hashCode() {
      return left.hashCode() ^ right.hashCode();
    }

    @Override
    public boolean equals(Object o) {
      if (!(o instanceof Pair))
        return false;
      @SuppressWarnings("rawtypes")
      Pair pairo = (Pair) o;
      return this.left.equals(pairo.getLeft()) && this.right.equals(pairo.getRight());
    }

  }

  /**
   * Entry point for search service output creation
   * 
   * @param search_string string to search for
   * @param match_type match type to use (exact,included or regex)
   * @param terminology_list list of terminologies in which to search
   * @return ArrayList of result json objects
   */
  public ArrayList<JsonObject> search(String search_string, String match_type,
      List<String> terminology_list) {
    String sparql;

    if (terminology_list.isEmpty()) {
      LOGGER.info("Search query: " + search_string + " match type: " + match_type
          + " over all internal terminologies");
    } else {
      LOGGER.info("Search query: " + search_string + " match type: " + match_type
          + " over the following terminologies: " + terminology_list.toString());
    }
    try {
      sparql = SPARQLQueryBuilder.getSPARQLSearch(terminology_list, match_type, search_string);
      vqe = VirtuosoQueryExecutionFactory.create(sparql, set);
      LOGGER.info("Executing SPARQL query: " + sparql);
    } catch (JenaException e) {
      if (e.getCause() == null) {
        throw new GFBioBadRequestException();
      } else if (e.getCause().toString().contains("Virtuoso")) {
        throw new GFBioServiceUnavailableException();
      } else {
        throw new GFBioInternalException();
      }
    } catch (Exception e) {
      throw new GFBioInternalException();
    }
    return createSearchResult();
  }

  /**
   * Entry point for search service output creation if there is only one terminology to search in
   * 
   * @param search_string string to search for
   * @param match_type match type to use (exact,included or regex)
   * @param terminology_list list of terminologies in which to search
   * @return ArrayList of result json objects
   */
  public ArrayList<JsonObject> search(String search_string, String match_type, String terminology) {
    ArrayList<String> a = new ArrayList<String>();
    if (!terminology.equals("")) {
      a.add(terminology.toUpperCase());
    }
    return search(search_string, match_type, a);
  }

  public GFBioResultSet<CapabilitiesResultSetEntry> capabilities(String terminology_id) {
    GFBioResultSet<CapabilitiesResultSetEntry> rs =
        new GFBioResultSet<CapabilitiesResultSetEntry>(terminology_id);
    CapabilitiesResultSetEntry e = new CapabilitiesResultSetEntry();

    String sparql = SPARQLQueryBuilder.getSPARQLClassesNumber(terminology_id);
    vqe = VirtuosoQueryExecutionFactory.create(sparql, set);
    ResultSet res = vqe.execSelect();
    int classesCount = Integer.parseInt(res.next().get("number").toString());

    ArrayList<String> servicesArray = new ArrayList<String>();
    for (Services s : availableServices) {
      if (s.toString().contains("allterms"))
        if (classesCount > limitAlltermsService)
          continue;
      servicesArray.add(s.toString());
    }
    e.setAvailableServices(servicesArray);

    ArrayList<String> modesArray = new ArrayList<String>();
    for (SearchModes m : searchModes) {
      modesArray.add(m.toString());
    }
    e.setSearchModes(modesArray);
    rs.addEntry(e);
    vqe.close();
    return rs;
  }

  public ArrayList<JsonObject> mappings(String terminologies_string) {
    ArrayList<JsonObject> mappings = new ArrayList<JsonObject>();
    String sparql;
    ParameterList terminology_list = new ParameterList(terminologies_string);
    if (terminology_list.size() != 2) {
      JsonObjectBuilder builder = Json.createObjectBuilder();
      builder.add("warning", "You should specify two terminologies as parameter.");
      mappings.add(builder.build());
      return mappings;
    } else {
      sparql = SPARQLQueryBuilder.getMappedURIs(terminology_list);
      // LOGGER.info("Executing SPARQL query: " + sparql);
      HashMap<String, String> mappedUris = new HashMap<String, String>();
      vqe = VirtuosoQueryExecutionFactory.create(sparql, set);
      ResultSet rs = vqe.execSelect();
      while (rs.hasNext()) {
        // extracing uris from Mappings graph
        QuerySolution qs = rs.next();
        mappedUris.put(qs.get("s").asResource().toString(),
            qs.get("slabel") != null ? qs.get("slabel").asLiteral().getString() : "n.a.");
      }
      vqe.close();
      JsonObjectBuilder builder = Json.createObjectBuilder();
      for (String termUri : mappedUris.keySet()) {
        String mappingKey = null, mappedTo;
        builder.add("uri1", termUri);
        builder.add("label1", mappedUris.get(termUri));
        sparql = SPARQLQueryBuilder.getSPARQLMappings(termUri);
        // LOGGER.info("Executing SPARQL query: " + sparql);
        vqe = VirtuosoQueryExecutionFactory.create(sparql, set);
        rs = vqe.execSelect();
        while (rs.hasNext()) {
          QuerySolution qs_mappings = rs.next();
          String pred = qs_mappings.get("p").asResource().toString();
          if (pred.equalsIgnoreCase("gfbiots:hasMappingKey")) {
            mappingKey = qs_mappings.get("o").asResource().toString();
            // builder.add("mappingKey", mappingKey);
          }
          if (pred.equalsIgnoreCase("gfbiots:isMappedTo")) {
            mappedTo = qs_mappings.get("o").asResource().toString();
            builder.add("uri2", mappedTo);
            builder.add("label2",
                mappedUris.get(mappedTo) != null ? mappedUris.get(mappedTo) : "n.a.");
          }
        }
        vqe.close();
        // get similarity measure
        if (mappingKey != null) {
          sparql = SPARQLQueryBuilder.getMappingMeasure(mappingKey);

          // LOGGER.info("Executing SPARQL query: " + sparql);

          vqe = VirtuosoQueryExecutionFactory.create(sparql, set);
          rs = vqe.execSelect();
          while (rs.hasNext()) {
            QuerySolution qs_mappings = rs.next();
            float measure = qs_mappings.get("o").asLiteral().getFloat();
            builder.add("measure", measure);
          }
          vqe.close();
        }

        mappings.add(builder.build());
      }
    }

    return mappings;

  }


  /**
   * Create output of the mappings service
   * 
   * @param terminologies_string list of terminology acronyms
   * @return ArrayList of result json objects
   */
  public ArrayList<JsonObject> mappingsNKA(String terminologies_string) {
    ArrayList<JsonObject> mappings = new ArrayList<JsonObject>();
    String sparql;
    ParameterList terminology_list = new ParameterList(terminologies_string);
    LOGGER.info("Size: " + terminology_list.size());
    if (terminology_list.size() != 2) {
      JsonObjectBuilder builder = Json.createObjectBuilder();
      builder.add("warning", "You should specify two terminologies as parameter.");
      mappings.add(builder.build());
      return mappings;
    }
    try {
      int index;
      sparql = SPARQLQueryBuilder.getSPARQLMappingsNKA(terminology_list);
      vqe = VirtuosoQueryExecutionFactory.create(sparql, set);
      LOGGER.info("Executing SPARQL query: " + sparql);
      ResultSet rs = vqe.execSelect();

      while (rs.hasNext()) {
        QuerySolution current_result = rs.next();
        JsonObjectBuilder builder = Json.createObjectBuilder();
        builder.add("uri1", current_result.get("uri1").toString());
        String label1 = "";
        if (current_result.get("label1") == null) {
          index = current_result.get("uri1").toString().lastIndexOf("/");
          label1 = current_result.get("uri2").toString().substring(index + 1);
          String[] labels1 = label1.split("(?<!(^|[A-Z]))(?=[A-Z])|(?<!^)(?=[A-Z][a-z])");
          label1 = ConvertStringArrayToString(labels1).toLowerCase();
        } else
          label1 = current_result.get("label1").as(Literal.class).getLexicalForm().toString();
        builder.add("label1", label1);
        builder.add("uri2", current_result.get("uri2").toString());
        String label2 = "";
        if (current_result.get("label2") == null) {
          index = current_result.get("uri2").toString().lastIndexOf("/");
          label2 = current_result.get("uri2").toString().substring(index + 1);
          String[] labels2 = label2.split("(?<!(^|[A-Z]))(?=[A-Z])|(?<!^)(?=[A-Z][a-z])");
          label2 = ConvertStringArrayToString(labels2).toLowerCase();
        } else
          label2 = current_result.get("label2").as(Literal.class).getLexicalForm().toString();
        builder.add("label2", label2);
        builder.add("measure", current_result.get("measure").asLiteral().getFloat());
        builder.add("relation", current_result.get("relation").asLiteral().toString());
        builder.add("wasGeneratedBy", "SoftwareAgent");
        mappings.add(builder.build());
      }
      // } catch (JenaException e) {
      // if (e.getCause() == null) {
      // throw new GFBioBadRequestException();
      // } else if (e.getCause().toString().contains("Virtuoso")) {
      // throw new GFBioServiceUnavailableException();
      // } else {
      // throw new GFBioInternalException();
      // }
    } catch (Exception e) {
      LOGGER.error(e.getLocalizedMessage());
      throw new GFBioInternalException();
    }
    vqe.close();
    return mappings;
  }

  static String ConvertStringArrayToString(String[] array) {
    // Concatenate all the elements into a StringBuilder.
    StringBuilder builder = new StringBuilder();
    for (String value : array) {
      builder.append(value);
      builder.append(' ');
    }
    return builder.toString().trim();
  }
}
