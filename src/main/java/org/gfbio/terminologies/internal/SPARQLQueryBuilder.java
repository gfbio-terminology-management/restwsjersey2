
package org.gfbio.terminologies.internal;

import java.util.HashMap;
import java.util.List;
import org.apache.log4j.Logger;
import org.gfbio.db.VirtGraphSingleton;
import org.gfbio.terminologies.environment.StandardVariables;
import org.gfbio.terminologies.exceptions.GFBioBadRequestException;
import org.gfbio.terminologies.exceptions.GFBioInternalException;
import org.gfbio.terminologies.exceptions.GFBioServiceUnavailableException;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.rdf.model.RDFNode;
import virtuoso.jena.driver.VirtGraph;
import virtuoso.jena.driver.VirtuosoQueryExecution;
import virtuoso.jena.driver.VirtuosoQueryExecutionFactory;

/**
 *
 * The SPARQL queries are using namespaces that are defined in the virtuoso server. namely: omv:
 * http://omv.ontoware.org/ontology# and gfbio: http://terminologies.gfbio.org/terms/ontology#
 *
 */

public class SPARQLQueryBuilder {

  private static final Logger LOGGER = Logger.getLogger(SPARQLQueryBuilder.class);

  private static final String uriPrefix = StandardVariables.getUriprefix();
  private static final String metadataSchema = StandardVariables.getMetadaschema();
  private static final String skos = "http://terminologies.gfbio.org/terms/ontology#SKOS";
  private static final String owl = "http://omv.ontoware.org/2005/05/ontology#OWL";

  // Temporary names for the properties. May change later.
  private static final String classesNumber = "numberOfClasses";
  private static final String individualNumber = StandardVariables.getIndividualnumber();
  private static final String maximumNumberOfChildren =
      StandardVariables.getMaximumnumberofchildren();
  private static final String maximumDepth = StandardVariables.getMaximumdepth();
  private static final String twentyfiveChildren = StandardVariables.getTwentyfivechildren();
  private static final String averageNumberOfChildren =
      StandardVariables.getAveragenumberofchildren();
  private static final String propertyNumber = StandardVariables.getPropertynumber();
  private static final String singleChild = StandardVariables.getSinglechild();
  private static final String classesWithoutDefinition =
      StandardVariables.getClasseswithoutdefinition();
  private static final String numberOfleaves = StandardVariables.getNumberofleaves();
  private static final String classesWithoutLabel = StandardVariables.getClasseswithoutlabel();
  private static final String classesWithMoreThan1Parent =
      StandardVariables.getClasseswithmorethan1parent();

  private static String metadataGraph = StandardVariables.getMetadataGraph();

  public static String getSPARQLAllTerms(String terminology_id) {
    StringBuilder query = new StringBuilder();
    int hash = terminology_id.hashCode();
    String metadata_query = "SELECT ?lang ?graph ?label FROM <" + uriPrefix + metadataGraph + ">"
        + "WHERE {<" + uriPrefix + hash + "> omv:hasOntologyLanguage ?lang." + "<" + uriPrefix
        + hash + "> gfbio:graph ?graph." + "<" + uriPrefix + hash
        + ">  gfbio:label ?label} LIMIT 100 ";
    VirtGraph set = VirtGraphSingleton.getInstance().getVirtGraph();
    VirtuosoQueryExecution vqe = VirtuosoQueryExecutionFactory.create(metadata_query, set);
    ResultSet res = vqe.execSelect();
    RDFNode lang = null;
    RDFNode graph = null;
    String label = "";
    if (res.hasNext()) {
      QuerySolution result = res.nextSolution();
      lang = result.get("lang");
      graph = result.get("graph");
      label = "<" + result.get("label").toString() + ">";
    }
    vqe.close();
    if (graph != null && lang != null) {

      query.append("SELECT DISTINCT ?uri ?label{");
      switch (lang.toString()) {
        case (owl):
          if (label.equals("")) {
            label = "rdfs:label";
          }
          query.append("{SELECT ?uri ?label FROM <" + graph.toString() + "> ");
          query.append("WHERE { ?uri a owl:Class . ?uri " + label + " ?label }}");
          query.append(" UNION ");
          query.append("{SELECT ?uri ?label FROM <" + graph.toString() + "> ");
          query.append("WHERE { ?uri a owl:DatatypeProperty . ?uri " + label + " ?label}}");
          query.append(" UNION ");
          query.append("{SELECT ?uri ?label FROM <" + graph.toString() + "> ");
          query.append("WHERE { ?uri a owl:ObjectProperty . ?uri " + label + " ?label}}");
          query.append(" UNION ");
          query.append("{SELECT ?ind as ?uri ?label FROM <" + graph.toString() + "> ");
          query.append(
              "WHERE { ?uri a owl:Class . ?ind rdf:type ?uri . ?ind " + label + " ?label}}");
          query.append("}");
          break;
        case (skos):
          if (label.equals("")) {
            label = "skos:prefLabel";
          }
          query.append("{SELECT ?uri ?label FROM <" + graph.toString() + "> ");
          query.append("WHERE { ?uri a skos:Concept . ?uri " + label + " ?label }}");
          query.append(" UNION ");
          query.append("{SELECT ?ind as ?uri ?label FROM <" + graph.toString() + "> ");
          query.append(
              "WHERE { ?uri a skos:Concept . ?ind rdf:type ?uri . ?ind " + label + " ?label}}");
          query.append("}");
          break;
      }
      query.append(" order by LCASE (?label)");
    }
    return query.toString();
  }

  public static String getSPARQLTerm(String terminology_id, String term_uri) {
    StringBuilder query = new StringBuilder();
    terminology_id = uriPrefix + terminology_id;
    query.append("SELECT ?attribute ?value " + "FROM <" + terminology_id + "> " + " WHERE {<"
        + term_uri + "> ?attribute ?value. FILTER ( ! isBLANK(?value))}");
    return query.toString();
  }

  public static String getSPARQLAllBroader(String terminology_id, String term_uri) {
    StringBuilder query = new StringBuilder();
    int hash = terminology_id.hashCode();
    String metadata_query = "SELECT ?lang ?graph ?label FROM <" + uriPrefix + metadataGraph + ">"
        + "WHERE {<" + uriPrefix + hash + "> omv:hasOntologyLanguage ?lang . " + "<" + uriPrefix
        + hash + "> gfbio:graph ?graph." + "<" + uriPrefix + hash + "> gfbio:label ?label}";
    VirtGraph set = VirtGraphSingleton.getInstance().getVirtGraph();
    VirtuosoQueryExecution vqe = VirtuosoQueryExecutionFactory.create(metadata_query, set);
    ResultSet res = vqe.execSelect();
    RDFNode lang = null;
    RDFNode graph = null;
    RDFNode label = null;
    while (res.hasNext()) {
      QuerySolution result = res.nextSolution();
      lang = result.get("lang");
      graph = result.get("graph");
      label = result.get("label");
    }
    if (graph != null && lang != null) {
      String hierarchyQueryBroader = "SELECT ?broaderProperty from <" + graph
          + "> where {?broaderProperty rdfs:subPropertyOf skos:broader}";
      vqe = VirtuosoQueryExecutionFactory.create(hierarchyQueryBroader, set);
      res = vqe.execSelect();
      RDFNode broaderProperty = null;
      if (res.hasNext()) {
        QuerySolution result = res.nextSolution();
        broaderProperty = result.get("broaderProperty");
      }
      String hierarchyQueryNarrower = "SELECT ?narrowerProperty from <" + graph
          + "> where {?narrowerProperty rdfs:subPropertyOf skos:narrower}";
      vqe = VirtuosoQueryExecutionFactory.create(hierarchyQueryNarrower, set);
      res = vqe.execSelect();
      RDFNode narrowerProperty = null;
      if (res.hasNext()) {
        QuerySolution result = res.nextSolution();
        narrowerProperty = result.get("narrowerProperty");
      }
      String hierarchyQuerySubClassOf = "SELECT ?subClassOfProperty from <" + graph
          + "> where {?subClassOfProperty rdfs:subPropertyOf rdfs:subClassOf}";
      vqe = VirtuosoQueryExecutionFactory.create(hierarchyQuerySubClassOf, set);
      res = vqe.execSelect();
      RDFNode subClassOfProperty = null;
      if (res.hasNext()) {
        QuerySolution result = res.nextSolution();
        subClassOfProperty = result.get("subClassOfProperty");
      }

      query.append("SELECT distinct ?broaderuri ?broaderlabel  FROM <" + graph + "> ");
      switch (lang.toString()) {
        case (skos):
          query.append("WHERE {{" + "SELECT distinct ?broaderuri ?broaderlabel FROM <" + graph + ">"
              + "WHERE{" + "?graphuri a  <http://www.w3.org/2004/02/skos/core#ConceptScheme>." + "<"
              + term_uri + "> (<" + broaderProperty + "> | skos:broader)* ?broaderuri."
              + "?broaderuri ?p ?broaderlabel. FILTER(?p = skos:prefLabel || ?p = <" + label + ">)"
              + "}} " + "UNION " + "{SELECT distinct ?broaderuri ?broaderlabel FROM <" + graph
              + "> " + "WHERE{ "
              + "?graphuri a  <http://www.w3.org/2004/02/skos/core#ConceptScheme>."
              + "?broaderUri (<" + narrowerProperty + "> | skos:narrower)* <" + term_uri + ">."
              + "?broaderuri ?p ?broaderlabel. FILTER(?p = skos:prefLabel || ?p = <" + label + ">)"
              + "}}}");
          break;
        case (owl):
          query.append("WHERE {" + "{SELECT distinct ?broaderuri ?broaderlabel FROM <" + graph
              + "> " + "WHERE {" + "?graphuri a  <http://www.w3.org/2002/07/owl#Ontology>." + "<"
              + term_uri + "> (<" + subClassOfProperty + "> | rdfs:subClassOf)+ ?broaderuri."
              + "?broaderuri ?p ?broaderlabel. FILTER(?p = rdfs:label || ?p = <" + label + ">)}}"
              + "UNION" + "{SELECT distinct ?broaderuri ?broaderlabel FROM <" + graph + ">"
              + "WHERE {<" + term_uri + "> rdf:type ?type . ?type rdfs:subClassOf+ ?broaderuri."
              + "?broaderuri ?p ?broaderlabel. FILTER(?p = rdfs:label || ?p = <" + label + ">)}}}");
          break;
      }
    }
    vqe.close();
    LOGGER.info(query.toString());
    return query.toString();
  }

  public static String getSPARQLAllNarrower(String terminology_id, String term_uri) {
    StringBuilder query = new StringBuilder();
    int hash = terminology_id.hashCode();
    String metadata_query =
        "SELECT distinct ?lang ?graph ?label FROM <" + uriPrefix + metadataGraph + ">" + "WHERE {<"
            + uriPrefix + hash + "> omv:hasOntologyLanguage ?lang . " + "<" + uriPrefix + hash
            + "> gfbio:graph ?graph ." + "<" + uriPrefix + hash + "> gfbio:label ?label}";
    VirtGraph set = VirtGraphSingleton.getInstance().getVirtGraph();
    VirtuosoQueryExecution vqe = VirtuosoQueryExecutionFactory.create(metadata_query, set);
    ResultSet res = vqe.execSelect();
    RDFNode lang = null;
    RDFNode graph = null;
    RDFNode label = null;
    while (res.hasNext()) {
      QuerySolution result = res.nextSolution();
      lang = result.get("lang");
      graph = result.get("graph");
      label = result.get("label");
    }
    if (graph != null && lang != null) {
      String hierarchyQuery = "SELECT ?broaderUri ?narrowerUri from <" + graph
          + "> where {?broaderUri rdfs:subPropertyOf skos:broader . ?narrowerUri rdfs:subPropertyOf skos:narrower}";
      vqe = VirtuosoQueryExecutionFactory.create(hierarchyQuery, set);
      res = vqe.execSelect();
      RDFNode broaderUri = null;
      RDFNode narrowerUri = null;
      if (res.hasNext()) {
        QuerySolution result = res.nextSolution();
        broaderUri = result.get("broaderUri");
        narrowerUri = result.get("narrowerUri");
      }

      String hierarchyQuerySubClassOf = "SELECT ?subClassOfProperty from <" + graph
          + "> where {?subClassOfProperty rdfs:subPropertyOf rdfs:subClassOf}";
      vqe = VirtuosoQueryExecutionFactory.create(hierarchyQuerySubClassOf, set);
      res = vqe.execSelect();
      RDFNode subClassOfProperty = null;
      if (res.hasNext()) {
        QuerySolution result = res.nextSolution();
        subClassOfProperty = result.get("subClassOfProperty");
      }

      query.append("SELECT distinct ?narroweruri ?narrowerlabel FROM <" + graph.toString() + "> ");
      switch (lang.toString()) {
        case (skos):
          query.append("WHERE {{" + "SELECT distinct ?narroweruri ?narrowerlabel FROM <" + graph
              + ">" + "WHERE{" + "?graphuri a  <http://www.w3.org/2004/02/skos/core#ConceptScheme>."
              + "<" + term_uri + "> (<" + narrowerUri + "> | skos:narrower)+ ?narroweruri."
              + "?narroweruri ?p ?narrowerlabel. FILTER(?p = skos:prefLabel || ?p = <" + label
              + ">)" + "}} " + "UNION " + "{SELECT distinct ?narroweruri ?narrowerlabel FROM <"
              + graph + "> " + "WHERE{ "
              + "?graphuri a  <http://www.w3.org/2004/02/skos/core#ConceptScheme>."
              + "?narroweruri (<" + broaderUri + "> | skos:broader)+ <" + term_uri + ">."
              + "?narroweruri ?p ?narrowerlabel. FILTER(?p = skos:prefLabel || ?p = <" + label
              + ">)" + "}}}");
          break;
        case (owl):
          query.append("WHERE {" + "{SELECT distinct ?narroweruri ?narrowerlabel FROM <" + graph
              + "> " + "WHERE {" + "?graphuri a  <http://www.w3.org/2002/07/owl#Ontology>."
              + "?narroweruri (<" + subClassOfProperty + "> | rdfs:subClassOf)+ <" + term_uri + ">."
              + "?narroweruri ?p ?narrowerlabel. FILTER(?p = rdfs:label || ?p = <" + label + ">)}}"
              + "UNION" + "{SELECT distinct ?narroweruri ?narrowerlabel FROM <" + graph + ">"
              + "WHERE {<" + term_uri + "> rdf:type ?type . ?narroweruri rdfs:subClassOf+ ?type."
              + "?narroweruri ?p ?narrowerlabel. FILTER(?p = rdfs:label || ?p = <" + label
              + ">)}}}");
          break;
      }
    }
    vqe.close();
    return query.toString();
  }

  public static String getSPARQLBroader(String terminology_id, String term_uri) {
    StringBuilder query = new StringBuilder();
    int hash = terminology_id.hashCode();
    String metadata_query = "SELECT ?lang ?graph ?label FROM <" + uriPrefix + metadataGraph + ">"
        + "WHERE {<" + uriPrefix + hash + "> omv:hasOntologyLanguage ?lang . " + "<" + uriPrefix
        + hash + "> gfbio:graph ?graph ." + "<" + uriPrefix + hash + "> gfbio:label ?label}";
    VirtGraph set = VirtGraphSingleton.getInstance().getVirtGraph();
    VirtuosoQueryExecution vqe = VirtuosoQueryExecutionFactory.create(metadata_query, set);
    ResultSet res = vqe.execSelect();
    RDFNode lang = null;
    RDFNode graph = null;
    RDFNode label = null;
    while (res.hasNext()) {
      QuerySolution result = res.nextSolution();
      lang = result.get("lang");
      graph = result.get("graph");
      label = result.get("label");
    }
    if (graph != null && lang != null) {
      query.append("SELECT distinct ?broaderuri ?broaderlabel  FROM <" + graph.toString() + "> ");
      switch (lang.toString()) {
        case (skos):
          query.append("WHERE {" + "{SELECT distinct ?broaderuri ?broaderlabel  FROM <"
              + graph.toString() + "> "
              + " WHERE{ ?graphuri a  <http://www.w3.org/2004/02/skos/core#ConceptScheme>." + "<"
              + term_uri + "> ?h ?broaderuri. ?h rdfs:subPropertyOf* skos:broader."
              + "?broaderuri ?p ?broaderlabel. FILTER(?p = skos:prefLabel || ?p = <" + label + ">)"
              + "}}" + "UNION" + "{SELECT distinct ?broaderuri ?broaderlabel  FROM <"
              + graph.toString() + "> "
              + " WHERE{ ?graphuri a  <http://www.w3.org/2004/02/skos/core#ConceptScheme>."
              + "?broaderuri ?h <" + term_uri + "> . ?h rdfs:subPropertyOf* skos:narrower."
              + "?broaderuri ?p ?broaderlabel. FILTER(?p = skos:prefLabel || ?p = <" + label + ">)"
              + "}}}");
          break;
        case (owl):
          query.append("WHERE {" + "{SELECT distinct ?broaderuri ?broaderlabel FROM <"
              + graph.toString() + "> " + "WHERE {"
              + "?graphuri a  <http://www.w3.org/2002/07/owl#Ontology>." + "<" + term_uri
              + ">  ?h ?broaderuri. ?h rdfs:subPropertyOf* rdfs:subClassOf."
              + "?broaderuri ?p ?broaderlabel. FILTER(?p = rdfs:label || ?p = <" + label + ">)}}"
              + "UNION" + "{SELECT distinct ?broaderuri ?broaderlabel FROM <" + graph.toString()
              + "> " + "WHERE {<" + term_uri
              + "> rdf:type ?type . ?type rdfs:subClassOf ?broaderuri."
              + "?broaderuri ?p ?broaderlabel. FILTER(?p = rdfs:label || ?p = <" + label + ">)}}}");
          break;
      }
    }
    vqe.close();
    return query.toString();
  }

  public static String getSPARQLNarrower(String terminology_id, String term_uri) {
    StringBuilder query = new StringBuilder();
    int hash = terminology_id.hashCode();
    String metadata_query = "SELECT ?lang ?graph ?label FROM <" + uriPrefix + metadataGraph + ">"
        + "WHERE {<" + uriPrefix + hash + "> omv:hasOntologyLanguage ?lang . " + "<" + uriPrefix
        + hash + "> gfbio:graph ?graph." + "<" + uriPrefix + hash + "> gfbio:label ?label}";
    VirtGraph set = VirtGraphSingleton.getInstance().getVirtGraph();
    VirtuosoQueryExecution vqe = VirtuosoQueryExecutionFactory.create(metadata_query, set);
    ResultSet res = vqe.execSelect();
    RDFNode lang = null;
    RDFNode graph = null;
    RDFNode label = null;
    while (res.hasNext()) {
      QuerySolution result = res.nextSolution();
      lang = result.get("lang");
      graph = result.get("graph");
      label = result.get("label");
    }
    if (graph != null && lang != null) {
      query.append("SELECT distinct ?narroweruri ?narrowerlabel FROM <" + graph.toString() + "> ");
      switch (lang.toString()) {
        case (skos):
          query.append("WHERE{" + "{SELECT distinct ?narroweruri ?narrowerlabel  FROM <"
              + graph.toString() + "> "
              + " WHERE{ ?graphuri a  <http://www.w3.org/2004/02/skos/core#ConceptScheme>." + "<"
              + term_uri + "> ?h ?narroweruri. ?h rdfs:subPropertyOf* skos:narrower."
              + "?narroweruri ?p ?narrowerlabel. FILTER(?p = skos:prefLabel || ?p = <" + label
              + ">)" + "}}" + "UNION" + "{SELECT distinct ?narroweruri ?narrowerlabel  FROM <"
              + graph.toString() + "> "
              + " WHERE{ ?graphuri a  <http://www.w3.org/2004/02/skos/core#ConceptScheme>."
              + "?narroweruri ?h <" + term_uri + "> . ?h rdfs:subPropertyOf* skos:broader."
              + "?narroweruri ?p ?narrowerlabel. FILTER(?p = skos:prefLabel || ?p = <" + label
              + ">)" + "}}}");
          break;
        case (owl):
          query.append("WHERE {" + "{SELECT distinct ?narroweruri ?narrowerlabel FROM <"
              + graph.toString() + "> " + "WHERE {"
              + "?graphuri a  <http://www.w3.org/2002/07/owl#Ontology>." + "?narroweruri ?h <"
              + term_uri + ">. ?h rdfs:subPropertyOf* rdfs:subClassOf."
              + "?narroweruri ?p ?narrowerlabel. FILTER(?p = rdfs:label || ?p = <" + label + ">)}}"
              + "UNION" + "{SELECT distinct ?narroweruri ?narrowerlabel <" + graph.toString() + "> "
              + "WHERE {<" + term_uri + "> rdf:type ?type . ?narroweruri rdfs:subClassOf ?type."
              + "?narroweruri ?p ?narrowerlabel. FILTER(?p = rdfs:label || ?p = <" + label
              + ">)}}}");
          break;
      }
    }
    vqe.close();
    LOGGER.info("Processing SPARQL " + query.toString());
    return query.toString();
  }

  public static String getSPARQLSynonym(String terminology_id, String term_uri) {
    StringBuilder query = new StringBuilder();
    int hash = terminology_id.hashCode();
    String metadata_query = "SELECT  ?graph ?synonym FROM <" + uriPrefix + metadataGraph + ">"
        + "WHERE {<" + uriPrefix + hash + "> gfbio:graph ?graph ." + "<" + uriPrefix + hash
        + "> gfbio:synonym ?synonym}";
    VirtGraph set = VirtGraphSingleton.getInstance().getVirtGraph();
    VirtuosoQueryExecution vqe = VirtuosoQueryExecutionFactory.create(metadata_query, set);
    ResultSet res = vqe.execSelect();
    RDFNode graph = null;
    RDFNode synonym = null;
    if (res.hasNext()) {
      QuerySolution result = res.nextSolution();
      graph = result.get("graph");
      synonym = result.get("synonym");
    }
    if (synonym != null) {
      query.append("SELECT ?synonym FROM <" + graph.toString() + "> " + "WHERE {<" + term_uri
          + "> ?p ?synonym. FILTER(?p = <" + synonym.toString() + ">" + "|| ?p = skos:altLabel)}");
    }
    vqe.close();
    return query.toString();
  }

  public static String getSPARQLMetadata(String terminology_id) {
    StringBuilder query = new StringBuilder();
    int hash = terminology_id.hashCode();
    String metadata_query = "SELECT ?lang ?graph FROM <" + uriPrefix + metadataGraph + ">"
        + "WHERE {<" + uriPrefix + hash + "> omv:hasOntologyLanguage ?lang . " + "<" + uriPrefix
        + hash + "> gfbio:graph ?graph}";
    VirtGraph set = VirtGraphSingleton.getInstance().getVirtGraph();
    VirtuosoQueryExecution vqe = VirtuosoQueryExecutionFactory.create(metadata_query, set);
    ResultSet res = vqe.execSelect();
    RDFNode lang = null;
    RDFNode graph = null;
    while (res.hasNext()) {
      QuerySolution result = res.nextSolution();
      lang = result.get("lang");
      graph = result.get("graph");
    }
    if (graph != null && lang != null) {
      query.append("SELECT ?attribute ?value FROM <" + graph.toString() + "> ");
      switch (lang.toString()) {
        case (skos):
          query.append("Where" + "{{?uri a <http://www.w3.org/2004/02/skos/core#ConceptScheme> }"
              + ".?uri ?attribute ?value. }");
          break;
        case (owl):
          query.append("Where {{ ?uri a <http://www.w3.org/2002/07/owl#Ontology> }"
              + ".?uri ?attribute ?value. }");
          break;
      }
    }
    vqe.close();
    return query.toString();
  }

  public static String getSPARQLTerminologies() {
    StringBuilder query = new StringBuilder("SELECT ?acronym ?uri ?name ?description " + "FROM <"
        + uriPrefix + metadataGraph + "> " + "WHERE {?s a omv:Ontology . ?s omv:acronym ?acronym . "
        + "?s gfbio:graph ?graph . ?s omv:URI ?uri . ?s omv:name ?name . ?s omv:description ?description}");
    return query.toString();
  }

  public static String getSPARQLMetrics(String terminology_id) {
    StringBuilder query = new StringBuilder();
    int hash = terminology_id.hashCode();
    query.append("SELECT ?attribute ?value ?label")
        .append(" FROM <" + uriPrefix + metadataGraph + ">")
        .append(" FROM <" + metadataSchema + ">")
        .append(" FROM <" + StandardVariables.getOwluri() + ">")
        .append(" FROM <http://omv.ontoware.org/2005/05/ontology>")
        .append(" WHERE {<" + uriPrefix + hash + "> ?attribute ?value. ")
        .append(" FILTER (?attribute = omv:" + classesNumber + "|| ?attribute = omv:"
            + individualNumber + "|| ?attribute = omv:" + propertyNumber + "|| ?attribute = gfbio:"
            + maximumDepth + "|| ?attribute = gfbio:" + maximumNumberOfChildren
            + "|| ?attribute = gfbio:" + averageNumberOfChildren + "|| ?attribute = gfbio:"
            + twentyfiveChildren + "|| ?attribute = gfbio:" + singleChild + "|| ?attribute = gfbio:"
            + classesWithoutDefinition + "|| ?attribute = gfbio:" + numberOfleaves
            + "|| ?attribute = gfbio:" + classesWithMoreThan1Parent + "|| ?attribute = gfbio:"
            + classesWithoutLabel + ")")
        .append(" . OPTIONAL{?attribute rdfs:label ?label}}");
    return query.toString();
  }

  public static String getSPARQLTerminologyInfo(String terminology_id) {
    StringBuilder query = new StringBuilder();
    int hash = terminology_id.hashCode();
    query.append("SELECT ?attribute ?value ?label")
        .append(" FROM <" + uriPrefix + metadataGraph + ">")
        .append(" FROM <" + metadataSchema + ">")
        .append(" FROM <" + StandardVariables.getOwluri() + ">")
        .append(" FROM <http://omv.ontoware.org/2005/05/ontology>").append(
            " WHERE {<" + uriPrefix + hash + "> ?attribute ?value. " + "FILTER (!?attribute = omv:"
                + classesNumber + "&& !?attribute = omv:" + individualNumber
                + "&& !?attribute = omv:" + propertyNumber + "&& !?attribute = gfbio:"
                + maximumDepth + "&& !?attribute = gfbio:" + maximumNumberOfChildren
                + "&& !?attribute = gfbio:" + averageNumberOfChildren + "&& !?attribute = gfbio:"
                + twentyfiveChildren + "&& !?attribute = gfbio:" + singleChild
                + "&& !?attribute = gfbio:" + classesWithoutDefinition + "&& !?attribute = gfbio:"
                + numberOfleaves + "&& !?attribute = gfbio:" + classesWithMoreThan1Parent
                + "&& !?attribute = gfbio:" + classesWithoutLabel + "&& !?attribute = gfbio:graph"
                + "&& !?attribute = gfbio:label" + "&& !?attribute = gfbio:definition"
                + "&& !?attribute = gfbio:synonym" + "&& !?attribute = omv:resourceLocator"
                + "&& !?attribute = rdf:type" + ")" + " . OPTIONAL{?attribute rdfs:label ?label}}");
    return query.toString();
  }

  public static String getSPARQLTerminologyArchive(String terminology_id) {
    StringBuilder query = new StringBuilder();
    query.append("SELECT * FROM <" + uriPrefix + "Metadata> WHERE {<" + uriPrefix
        + terminology_id.hashCode() + "> ?p ?o. FILTER(?p = gfbio:lastArchiveID) }");
    return query.toString();
  }

  public static String getSPARQLSuggestSearch(String search_string, List<String> terminology_list,
      int limit) {
    search_string = search_string.replaceAll("\"", "");
    // search_string_mod = ".*" + search_string_mod + ".*";
    String labelURIs = getSearchMetadata(false).get("labelURIs");
    StringBuilder query = new StringBuilder();
    query.append("SELECT DISTINCT ?uri, ?label, ?terminology, ?graph FROM <" + uriPrefix
        + metadataGraph + "> WHERE{ quad map virtrdf:DefaultQuadMap "
        + "{ graph ?graph { ?uri ?labeluri ?label .");

    query.append(" FILTER ( (?labeluri = rdfs:label " + labelURIs + ") "
        + "&& bif:contains(?label, '\"" + search_string + "*\"')). ");

    if (!terminology_list.isEmpty()) {
      String terminologies = "";
      for (String term_items : terminology_list) {
        terminologies += " <" + uriPrefix + term_items + "> ";
      }
      query.append("values ?graph {" + terminologies + "}.");
    }
    query.append("} .?o gfbio:graph ?graph. ?o omv:acronym ?terminology }}");
    query.append("ORDER BY STRLEN(?label)");
    query.append("LIMIT " + String.valueOf(limit));
    return query.toString();
  }

  public static String getSPARQLLabel(String terminology_id, String term_uri) {
    StringBuilder query = new StringBuilder();
    int hash = terminology_id.hashCode();
    String metadata_query = "SELECT ?lang ?graph ?label FROM <" + uriPrefix + metadataGraph + ">"
        + "WHERE {<" + uriPrefix + hash + "> omv:hasOntologyLanguage ?lang . " + "<" + uriPrefix
        + hash + "> gfbio:graph ?graph ." + "<" + uriPrefix + hash + "> gfbio:label ?label}";
    VirtGraph set = VirtGraphSingleton.getInstance().getVirtGraph();
    VirtuosoQueryExecution vqe = VirtuosoQueryExecutionFactory.create(metadata_query, set);
    ResultSet res = vqe.execSelect();
    RDFNode lang = null;
    RDFNode graph = null;
    RDFNode label = null;
    while (res.hasNext()) {
      QuerySolution result = res.nextSolution();
      lang = result.get("lang");
      graph = result.get("graph");
      label = result.get("label");
    }
    if (graph != null && lang != null) {
      query.append("SELECT ?label  FROM <" + graph + "> ");
      switch (lang.toString()) {
        case (skos):
          query.append("WHERE {" + "<" + term_uri
              + "> ?p ?label. FILTER(?p = skos:prefLabel || ?p = rdfs:label || ?p = <" + label
              + ">)" + "}");
          break;
        case (owl):
          query.append("WHERE {\n" + "<" + term_uri
              + "> ?p ?label. FILTER(?p = rdfs:label || ?p = <" + label + ">)" + "}");
          break;
      }
    }
    vqe.close();
    return query.toString();
  }

  public static String getSPARQLType(String terminology_id, String term_uri) {
    StringBuilder query = new StringBuilder();
    int hash = terminology_id.hashCode();
    String metadata_query = "SELECT ?lang ?graph  FROM <" + uriPrefix + metadataGraph + ">"
        + "WHERE {<" + uriPrefix + hash + "> omv:hasOntologyLanguage ?lang . " + "<" + uriPrefix
        + hash + "> gfbio:graph ?graph }";
    VirtGraph set = VirtGraphSingleton.getInstance().getVirtGraph();
    VirtuosoQueryExecution vqe = VirtuosoQueryExecutionFactory.create(metadata_query, set);
    ResultSet res = vqe.execSelect();
    RDFNode lang = null;
    RDFNode graph = null;
    while (res.hasNext()) {
      QuerySolution result = res.nextSolution();
      lang = result.get("lang");
      graph = result.get("graph");
    }
    if (graph != null && lang != null) {
      query.append("SELECT ?type  FROM <" + graph + "> ");
      query.append("WHERE {" + "<" + term_uri + "> rdf:type ?type}");
    }
    vqe.close();
    return query.toString();
  }

  /**
   * TODO
   * 
   * @param terminology_list
   * @param match_type
   * @param search_string
   * @return
   * @throws GFBioBadRequestException
   * @throws GFBioInternalException
   * @throws GFBioServiceUnavailableException
   */
  public static String getSPARQLSearch(List<String> terminology_list, String match_type,
      String search_string)
      throws GFBioBadRequestException, GFBioInternalException, GFBioServiceUnavailableException {

    String extensions = getSearchMetadata(true).get("extensions");
    String labelURIs = getSearchMetadata(true).get("labelURIs");
    String search_string_mod = search_string.replaceAll("\"", "");
    StringBuilder query = new StringBuilder();
    query.append("SELECT DISTINCT ?uri, ?terminology, ?graph, ?label, ?url ")
        .append(" FROM <" + uriPrefix + metadataGraph + "> ").append(
            " WHERE {{ quad map virtrdf:DefaultQuadMap { graph ?graph { ?uri ?labeluri ?label .");

    if (match_type.equals("exact")) {
      query.append(" ?label bif:contains \"'" + search_string + "'\". FILTER ( STRLEN(?label) = "
          + search_string_mod.length() + " && ?labeluri IN ( rdfs:label, skos:altLabel "
          + labelURIs.toString() + extensions.toString() + " ) ). ");
    } else if (match_type.equals("included")) {
      query.append(" ?label bif:contains \"'" + search_string
          + "'\". FILTER ( ?labeluri IN ( rdfs:label, skos:altLabel " + labelURIs.toString()
          + extensions.toString() + ") ) . ");
    } else if (match_type.equals("regex")) {
      query.append(" FILTER ( ?labeluri  IN ( rdfs:label, skos:altLabel " + labelURIs.toString()
          + extensions.toString() + " ) && regex($label, \"" + search_string_mod + "\")) . ");
    } else {
      throw new GFBioBadRequestException();
    }
    if (!terminology_list.isEmpty()) {
      String terminologies = "";
      for (String term_items : terminology_list) {
        terminologies += " <" + uriPrefix + term_items + "> ";
      }
      query.append("values ?graph {" + terminologies + "}.");
    }
    query.append(" OPTIONAL { ?uri <http://terminologies.gfbio.org/terms/ts-schema/url> ?url. }");
    query.append(
        "} ?o gfbio:graph ?graph. ?o omv:acronym ?terminology. OPTIONAL { ?uri <http://terminologies.gfbio.org/terms/ts-schema/url> ?url. }}}}");
    LOGGER.info("Processing SPARQL for search " + query.toString());
    return query.toString();
  }

  public static String getSPARQLClassesNumber(String terminology_id) {
    int hash = terminology_id.hashCode();
    StringBuilder sb = new StringBuilder();
    sb.append("SELECT ?number FROM <" + uriPrefix + metadataGraph + "> WHERE {");
    sb.append(
        "<" + StandardVariables.getUriprefix() + hash + "> omv:" + classesNumber + " ?number}");
    return sb.toString();
  }

  private static HashMap<String, String> getSearchMetadata(boolean commaSeparated) {
    HashMap<String, String> map = new HashMap<String, String>();
    VirtGraph set = VirtGraphSingleton.getInstance().getVirtGraph();
    StringBuilder extensions = new StringBuilder();
    StringBuilder labelURIs = new StringBuilder();

    String metadata_query_labels = "SELECT distinct ?label FROM <" + uriPrefix + metadataGraph + ">"
        + "WHERE { ?graph gfbio:label ?label}";

    String metadata_query_synonyms = "SELECT distinct ?synonym FROM <" + uriPrefix + metadataGraph
        + ">" + "WHERE { ?graph gfbio:synonym ?synonym}";

    String metadata_query_abbreviations = "SELECT distinct ?abbreviation FROM <" + uriPrefix
        + metadataGraph + ">" + "WHERE { ?graph gfbio:abbreviation ?abbreviation}";

    String metadata_query_symbols = "SELECT distinct ?symbol FROM <" + uriPrefix + metadataGraph
        + ">" + "WHERE {?graph gfbio:symbol ?symbol}";

    VirtuosoQueryExecution vqe = VirtuosoQueryExecutionFactory.create(metadata_query_synonyms, set);
    ResultSet res = vqe.execSelect();
    // ArrayList<String> usedURIs = new ArrayList<String>();
    while (res.hasNext()) {
      QuerySolution result = res.nextSolution();
      if (result.get("synonym") != null && !result.get("synonym").toString()
          .equals("http://www.w3.org/2004/02/skos/core#altLabel")) {
        if (commaSeparated == false) {
          extensions.append(" || ");
          extensions.append("?p = ");
        } else {
          extensions.append(",");
        }
        extensions.append("<" + result.get("synonym") + ">");
      }
    }
    vqe.close();
    //
    vqe = VirtuosoQueryExecutionFactory.create(metadata_query_abbreviations, set);
    res = vqe.execSelect();
    while (res.hasNext()) {
      QuerySolution result = res.nextSolution();
      if (result.get("abbreviation") != null) {
        if (commaSeparated == false) {
          extensions.append(" || ");
          extensions.append("?p = ");
        } else {
          extensions.append(",");
        }
        extensions.append("<" + result.get("abbreviation") + ">");

      }
    }
    vqe.close();
    //
    vqe = VirtuosoQueryExecutionFactory.create(metadata_query_labels, set);
    res = vqe.execSelect();
    while (res.hasNext()) {
      QuerySolution result = res.nextSolution();
      if (result.get("label") != null
          && !result.get("label").toString().equals("http://www.w3.org/2000/01/rdf-schema#label")) {
        if (commaSeparated == false) {
          labelURIs.append(" || ");
          labelURIs.append("?labeluri = ");
        } else {
          labelURIs.append(",");
        }
        labelURIs.append("<" + result.get("label") + ">");
      }
    }
    vqe.close();
    //
    vqe = VirtuosoQueryExecutionFactory.create(metadata_query_symbols, set);
    res = vqe.execSelect();
    while (res.hasNext()) {
      QuerySolution result = res.nextSolution();
      if (result.get("symbol") != null) {
        if (commaSeparated == false) {
          extensions.append(" || ");
          extensions.append("?p = ");
        } else {
          extensions.append(",");
        }
        extensions.append("<" + result.get("symbol") + ">");
      }
    }
    vqe.close();
    map.put("extensions", extensions.toString());
    map.put("labelURIs", labelURIs.toString());
    return map;
  }

  public static String getMappedURIs(List<String> terminologies) {
    String query = new StringBuilder().append("SELECT ?s ?slabel")
        .append(" FROM <" + uriPrefix + StandardVariables.getMappingsgraph() + ">")
        .append(" FROM <" + uriPrefix + terminologies.get(0) + ">")
        .append(" FROM <" + uriPrefix + terminologies.get(1) + ">").append(" WHERE { ?s ?p ?o .")
        .append(" FILTER(?o = <" + uriPrefix + terminologies.get(0) + "> || ?o = <" + uriPrefix
            + terminologies.get(1) + "> )")
        .append("OPTIONAL { ?s rdfs:label ?slabel } . }").toString();

    return query.toString();
  }

  // SELECT * FROM <http://terminologies.gfbio.org/terms/Mappings> WHERE {
  // <http://purl.obolibrary.org/obo/FLOPO_0003435> ?p ?o .}
  public static String getSPARQLMappings(String termUri) {
    String query = new StringBuilder().append("SELECT *")
        .append(" FROM <" + uriPrefix + StandardVariables.getMappingsgraph() + ">")
        .append(" WHERE { <" + termUri + "> ?p ?o . }").toString();

    return query.toString();
  }

  // SELECT * FROM <http://terminologies.gfbio.org/terms/Mappings> WHERE {
  // <http://terminologies.gfbio.org/terms/FLOPO_0003435_TO_0000858> ?p ?o . }
  public static String getMappingMeasure(String mappingKey) {
    String query = new StringBuilder().append("SELECT *")
        .append(" FROM <" + uriPrefix + StandardVariables.getMappingsgraph() + ">")
        .append(" WHERE { <" + mappingKey + "> ?p ?o . }").toString();

    return query.toString();
  }

  public static String getSPARQLMappingsNKA(List<String> terminology_list) {
    java.util.Collections.sort(terminology_list);
    String term1 = terminology_list.get(0);
    String term2 = terminology_list.get(1);

    String query = new StringBuilder()
        .append("SELECT ?uri1 ?label1 ?uri2 ?label2 str(?ms) AS ?measure ?relation")
        .append(" FROM <" + uriPrefix + StandardVariables.getMappingsgraph() + "/")
        .append(term1.toLowerCase() + "-" + term2.toLowerCase() + ">")
        .append(" FROM <" + uriPrefix + term1 + ">").append(" FROM <" + uriPrefix + term2 + ">")
        .append(" WHERE {")
        .append(" ?c <http://knowledgeweb.semanticweb.org/heterogeneity/alignmententity1> ?uri1.")
        .append(" ?c <http://knowledgeweb.semanticweb.org/heterogeneity/alignmententity2> ?uri2.")
        .append(" FILTER (?uri1 != ?uri2).").append(" ?uri1 rdfs:label ?label1.")
        .append(" ?c <http://knowledgeweb.semanticweb.org/heterogeneity/alignmentmeasure> ?ms .")
        .append(
            " ?c <http://knowledgeweb.semanticweb.org/heterogeneity/alignmentrelation> ?relation .")
        .append(" OPTIONAL { ?uri2 rdfs:label ?label2. }}").toString();

    return query.toString();
  }
}
