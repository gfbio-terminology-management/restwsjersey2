package org.gfbio.terminologies.internal;

import java.util.ArrayList;

import javax.ws.rs.WebApplicationException;

@SuppressWarnings("serial")
public class ParameterList extends ArrayList<String>{
    
    public ParameterList(){
        super();
    }
    public ParameterList(String s) throws NullPointerException{
        super();
        
        if (s.length() != 0)
        {
        if (!s.contains(",")) add(s.trim());
        else
        for (String v : s.split(",")) {
            try {
                add(v.trim());
            } catch (Exception ex) {
                ex.printStackTrace();
                throw new WebApplicationException(400);
            }
        }
        }
        
    }
}