package org.gfbio.terminologies.exceptions;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

@SuppressWarnings("serial")
public class GFBioBadRequestException extends WebApplicationException {

    /**
      * Create a HTTP 400 (Bad Request) exception.
     */
     public GFBioBadRequestException() {
         super(Response.status(Status.BAD_REQUEST).build());
     }

     /**
      * Create a HTTP 400 (Bad Request) exception with text.
      * @param message the String that is the entity of the 400 response.
      */
     public GFBioBadRequestException(String message) {
         super(Response.status(Status.BAD_REQUEST).entity(message).type("text/plain").build());
     }

}