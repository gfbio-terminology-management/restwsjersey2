package org.gfbio.terminologies.restws;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonString;
import javax.json.JsonValue;
import javax.json.JsonWriter;
import javax.json.JsonWriterFactory;
import javax.json.stream.JsonGenerator;
import org.gfbio.terminologies.environment.StandardVariables;
import org.gfbio.terminologies.internal.ParameterList;
import org.json.CDL;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;


@SuppressWarnings({"unchecked", "rawtypes"})
public class Formatter {

  private List<JsonObject> resultList = new ArrayList<JsonObject>();
  private JsonObjectBuilder prefixBuilder = Json.createObjectBuilder();
  private JsonObjectBuilder resultBuilder = Json.createObjectBuilder();
  private final String schemaBase = StandardVariables.getTSschema() + "/";
  String query;
  String format;
  ParameterList terms;

  HashMap<String, String> contextEntries = new HashMap();

  public Formatter(String format, String query) {
    this.query = query;
    this.format = format;
  }

  public String formatResultV2(JsonObject result) {

    if (result != null && !result.isEmpty()) {
      if (result.containsKey("context")) {

        for (String pair : result.get("context").toString().split(",")) {
          if (pair.equals("{}")) {
            break;
          }
          String[] split = pair.split("\":\"");
          contextEntries.put(split[0].replace("{\"", "").replace("\"", ""),
              split[1].replace("\"", "").replace("}", ""));
        }
      }
    }

    Map<String, Object> config = new HashMap<String, Object>(1);
    config.put(JsonGenerator.PRETTY_PRINTING, true);
    JsonWriterFactory factory = Json.createWriterFactory(config);
    StringWriter sw = new StringWriter();
    JsonWriter jsonwriter = factory.createWriter(sw);


    switch (format) {
      case "xml":
        JsonObject value2 = Json.createObjectBuilder().add("root", result).build();
        JSONObject json = new JSONObject(value2.toString());
        JSONObject json_cleaned = (JSONObject) cleanJsonForXmlExport(json);
        return XML.toString(json_cleaned);
      case "csv":
        JSONArray res = new JSONObject(result.toString()).getJSONArray("results");
        String csv = CDL.toString(res);
        return csv;
      case "jsonld":

        JsonObjectBuilder contextBuilder = Json.createObjectBuilder();

        Iterator iterator = result.keySet().iterator();
        while (iterator.hasNext()) {
          String key = (String) iterator.next();
          Object value = result.get(key);
          if (!key.equalsIgnoreCase("results") && !key.equalsIgnoreCase("context")) {
            contextBuilder.add(key, (JsonValue) value);
          }
        }

        // extract "results"
        JsonArrayBuilder graphBuilder = buildGraphV2(result.getJsonArray("results"));

        // build @context
        JsonObjectBuilder context = null;
        if (!contextEntries.isEmpty()) {
          context = Json.createObjectBuilder();
        }
        for (Map.Entry<String, String> entry : contextEntries.entrySet()) {
          context.add(entry.getKey(), entry.getValue());
        }
        if (context == null) {
          contextBuilder.add("@context", prefixBuilder.build());
        } else {
          contextBuilder.add("@context", context.build());
        }

        // build @graph
        if (graphBuilder == null) {
          contextBuilder.add("@graph", resultBuilder.build());
        } else {
          contextBuilder.add("@graph", graphBuilder.build());
        }

        jsonwriter.writeObject(contextBuilder.build());
        return sw.toString();
      default:
        jsonwriter.writeObject(result);
        return sw.toString();
    }
  }


  public String formatResult(List<JsonObject> resultlist) {
    try {
      if (resultlist != null) {
        this.resultList.addAll(resultlist);
      }
      Map<String, Object> config = new HashMap<String, Object>(1);
      config.put(JsonGenerator.PRETTY_PRINTING, true);
      JsonWriterFactory factory = Json.createWriterFactory(config);
      JsonArrayBuilder results_ab = Json.createArrayBuilder();
      JsonArrayBuilder diagnostics_ab = Json.createArrayBuilder();

      for (JsonObject result : resultList) {
        if (result != null && !result.isEmpty()) {
          if (result.containsKey("warning") || result.containsKey("error")) {
            diagnostics_ab.add(result);
          } else if (result.containsKey("context")) {
            // TODO find a better solution to get all the entries in context entries
            for (String pair : result.get("context").toString().split(",")) {
              if (pair.equals("{}")) {
                break;
              }
              String[] split = pair.split("\":\"");
              contextEntries.put(split[0].replace("{\"", "").replace("\"", ""),
                  split[1].replace("\"", "").replace("}", ""));
            }
          } else {
            results_ab.add(result);
          }
        }
      }

      JsonArray results = results_ab.build();
      JsonArray diagnostics = diagnostics_ab.build();

      JsonObject value = Json.createObjectBuilder()
          .add("request",
              Json.createObjectBuilder().add("query", query).add("executionTime",
                  new java.util.Date().toString()))
          .add("results", results).add("diagnostics", diagnostics).build();
      StringWriter sw = new StringWriter();
      JsonWriter jsonwriter = factory.createWriter(sw);
      jsonwriter.writeObject(value);
      switch (format) {
        case "xml":
          JsonObject value2 = Json.createObjectBuilder().add("root", value).build();
          JSONObject json = new JSONObject(value2.toString());
          JSONObject json_cleaned = (JSONObject) cleanJsonForXmlExport(json);
          return XML.toString(json_cleaned);
        case "csv":
          JSONArray res = new JSONObject(value.toString()).getJSONArray("results");
          String csv = CDL.toString(res);
          return csv;
        case "jsonld":
          JSONArray result = new JSONObject(value.toString()).getJSONArray("results");

          JsonArrayBuilder graphBuilder = buildGraph(result);



          JsonObjectBuilder contextBuilder = Json.createObjectBuilder();
          JsonObjectBuilder context = null;
          if (!contextEntries.isEmpty()) {
            context = Json.createObjectBuilder();
          }
          for (Map.Entry<String, String> entry : contextEntries.entrySet()) {
            context.add(entry.getKey(), entry.getValue());
          }
          if (context == null) {
            contextBuilder.add("@context", prefixBuilder.build());
          } else {
            contextBuilder.add("@context", context.build());
          }
          if (graphBuilder == null) {
            contextBuilder.add("@graph", resultBuilder.build());
          } else {
            contextBuilder.add("@graph", graphBuilder.build());
          }



          JsonObjectBuilder complete = Json.createObjectBuilder()
              .add("request",
                  Json.createObjectBuilder().add("query", query).add("executionTime",
                      new java.util.Date().toString()))
              .add("results", Json.createArrayBuilder().add(contextBuilder))
              .add("diagnostics", diagnostics);
          JsonObject completeObject = complete.build();
          StringWriter writer = new StringWriter();
          JsonWriter jwriter = factory.createWriter(writer);
          jwriter.writeObject(completeObject);
          return writer.toString();
        default:
          return sw.toString();
      }
    } catch (JSONException e) {
      return null;
    }
  }

  private Object cleanJsonForXmlExport(Object input) throws JSONException {
    if (input instanceof JSONObject) {
      JSONObject returnObject = new JSONObject();
      JSONObject jsonObject = (JSONObject) input;
      Iterator<?> keys = jsonObject.keys();
      while (keys.hasNext()) {
        String key = (String) keys.next();
        // remove all not-allowed characters from the string
        String correctedKey = key;
        correctedKey = key.replaceAll(
            "[^A-Z_a-z\\xC0-\\xD6\\xD8-\\xF6\\xF8-\\x{2FF}\\x{370}-\\x{37D}\\x{37F}-\\x{1FFF}\\x{200C}-\\x{200D}\\x{2070}-\\x{218F}\\x{2C00}-\\x{2FEF}\\x{3001}-\\x{D7FF}\\x{F900}-\\x{FDCF}\\x{FDF0}-\\x{FFFD}\\-\\.0-9\\xB7\\x{300}-\\x{36F}\\x{203F}-\\x{2040}]",
            "_");
        // remove all not-allowed characters from the beginning of the string
        correctedKey = correctedKey.replaceAll("^[\\-\\.0-9]+", "_");

        Object value = cleanJsonForXmlExport(jsonObject.get(key));
        returnObject.put(correctedKey, value);
      }
      return returnObject;
    } else if (input instanceof JSONArray) {
      JSONArray jsonArray = (JSONArray) input;
      JSONArray returnArray = new JSONArray();
      for (int i = 0; i < jsonArray.length(); i++) {
        Object o = jsonArray.get(i);
        Object value = cleanJsonForXmlExport(o);
        returnArray.put(value);
      }
      return returnArray;
    } else {
      return input;
    }
  }

  private JsonArrayBuilder buildGraph(JSONArray array) {
    try {
      JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();

      for (int i = 0; i < array.length(); i++) {
        JsonObjectBuilder graphBuilder = Json.createObjectBuilder();
        JSONObject jsonObject = array.getJSONObject(i);
        Iterator iterator = jsonObject.keys();
        while (iterator.hasNext()) {
          String key = (String) iterator.next();
          Object value = jsonObject.get(key);
          addDataToContextIfNotExists(key);
          if (value instanceof String) {
            graphBuilder.add(key, (String) value);
          } else if (value instanceof JSONArray) {
            JSONArray valueArray = (JSONArray) value;
            JsonArrayBuilder valueArrayBuilder = Json.createArrayBuilder();
            for (int j = 0; j < valueArray.length(); j++) {
              valueArrayBuilder.add(valueArray.getString(j));
            }
            graphBuilder.add(key, valueArrayBuilder);
          }
        }
        arrayBuilder.add(graphBuilder.build());
      }
      return arrayBuilder;
    } catch (JSONException e) {
      return null;
    } catch (NullPointerException e) {
      return null;
    }
  }

  private JsonArrayBuilder buildGraphV2(JsonArray array) {
    try {
      JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();

      for (int i = 0; i < array.size(); i++) {
        JsonObjectBuilder graphBuilder = Json.createObjectBuilder();
        JsonObject jsonObject = array.getJsonObject(i);
        Iterator iterator = jsonObject.keySet().iterator();
        while (iterator.hasNext()) {
          String key = (String) iterator.next();
          Object value = jsonObject.get(key);
          addDataToContextIfNotExists(key);
          if (value instanceof JsonString) {
            graphBuilder.add(key, (JsonValue) value);
          } else if (value instanceof JsonArray) {
            JsonArray valueArray = (JsonArray) value;
            JsonArrayBuilder valueArrayBuilder = Json.createArrayBuilder();
            for (int j = 0; j < valueArray.size(); j++) {
              valueArrayBuilder.add(valueArray.getString(j));
            }
            graphBuilder.add(key, valueArrayBuilder);
          }
        }
        arrayBuilder.add(graphBuilder.build());
      }
      return arrayBuilder;
    } catch (JSONException e) {
      return null;
    } catch (NullPointerException e) {
      return null;
    }
  }


  private void addDataToContextIfNotExists(String key) {
    if (!contextEntries.containsKey(key)) {
      contextEntries.put(key, schemaBase + key);
    }
  }
}
