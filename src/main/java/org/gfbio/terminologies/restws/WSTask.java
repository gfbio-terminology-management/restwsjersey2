package org.gfbio.terminologies.restws;

import java.util.List;
import java.util.concurrent.Callable;

import javax.json.JsonObject;

import org.apache.log4j.Logger;
import org.gfbio.interfaces.WSInterface;


public class WSTask implements Callable<List<JsonObject>> {
	
	private static final Logger LOGGER = Logger.getLogger(WSTask.class);

	
	private WSInterface wsInterface;
	private final String searchString;
	private final String matchType;
	
	public WSTask(WSInterface wsInterface, final String searchString, final String matchType) {
		this.wsInterface = wsInterface;
		this.searchString = searchString;
		this.matchType = matchType;
	}

	

	/**
	 * this method returns actually NULL also if no result
	 */
	@Override
	public List<JsonObject> call() throws Exception {
		LOGGER.info("Asynchronous execution begins ... " );

		List<JsonObject> transfer = this.wsInterface.search(searchString, matchType).create();
		LOGGER.info("Asynchronous execution started");

		int size = 0;
		if(transfer !=null) {
		size = transfer.size()-1; // -1 because of the context element
		} else {
			//FIXME here we can add a List with one JSON element, this JSON element containts at the label a errormessage 
		}
		LOGGER.info(wsInterface.getAcronym() +" Asynchronous execution finished, results added = " +size );
		return transfer;
	}
	
}
