package org.gfbio.terminologies.restws;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import org.apache.log4j.Logger;
import org.gfbio.config.ConfigSvc;
import org.gfbio.db.VirtGraphSingleton;
import org.gfbio.terminologies.environment.StandardVariables;
import org.gfbio.terminologies.environment.StandardVariables.TERMSERVICETYPES;
import org.gfbio.terminologies.exceptions.GFBioBadRequestException;
import org.gfbio.terminologies.exceptions.GFBioServiceUnavailableException;
import org.gfbio.terminologies.external.ExternalWS;
import org.gfbio.terminologies.internal.GFBioTS;
import com.hp.hpl.jena.query.ResultSet;
import virtuoso.jena.driver.VirtGraph;
import virtuoso.jena.driver.VirtuosoQueryExecution;
import webservice.DWBWSserviceImpl;

/**
 * Main Class for the GFBio Terminology Service Webservice implementation This class implements all
 * the TS endpoints.
 * 
 * @author nkaram <A HREF="mailto:naouel.karam@fu-berlin.de">Naouel Karam</A>
 * 
 */

@Path("terminologies")
public class GFBioTermWS {

  private static final Logger LOGGER = Logger.getLogger(GFBioTermWS.class);

  private SearchService searchService;
  private ExternalWS externalWS = ExternalWS.getInstance();
  private GFBioTS GFBioTSInstance = GFBioTS.getInstance();
  private ConfigSvc cfgSvc = ConfigSvc.getInstance(null);

  public GFBioTermWS() {
    this.searchService = new SearchService();
  }

  /**
   * Implements the terminologies endpoint. Calls the GFBioTSInstance terminologies method then
   * formats the results in the format passed as parameter (default json).
   * 
   * @param uriInfo
   * @param format
   * @return
   * @throws ServletException
   * @throws IOException
   */
  @GET
  @Path("")
  public Response getTerminologies(@Context final UriInfo uriInfo,
      @QueryParam("format") @DefaultValue("json") String format) throws IOException {
    final String calling_uri = uriInfo.getRequestUri().toString();
    final int truncIndex = calling_uri.indexOf("/terminologies");
    Formatter formatter =
        new Formatter(format, StandardVariables.getWsprefix() + calling_uri.substring(truncIndex));
    List<JsonObject> resultList;
    String res;
    try {
      resultList = GFBioTSInstance.terminologies();
      res = formatter.formatResult(resultList);
    } catch (Exception e) {
      Response r = handleException(e, format, formatter);
      return r;
    }
    return Response.ok(res).header(HttpHeaders.CONTENT_TYPE, getFormat(format)).build();
  }

  /**
   * Implements the terminology infos endpoint. Calls the GFBioTSInstance (for internal) or the
   * externalWS (for external) corresponding method then formats the results in the format passed as
   * parameter (default json).
   * 
   * @param uriInfo
   * @param terminology_id
   * @param format
   * @return
   * @throws ServletException
   * @throws IOException
   */
  @GET
  @Path("/{terminology_id}")
  public Response getTerminologyInfos(@Context final UriInfo uriInfo,
      @PathParam("terminology_id") String terminology_id,
      @QueryParam("format") @DefaultValue("json") String format) throws IOException {
    final String calling_uri = uriInfo.getRequestUri().toString();
    final int truncIndex = calling_uri.indexOf("/terminologies");
    Formatter formatter =
        new Formatter(format, StandardVariables.getWsprefix() + calling_uri.substring(truncIndex));
    List<JsonObject> resultList;
    String res;
    try {
      if (this.externalWS.contains(terminology_id)
          || terminology_id.contains(new DWBWSserviceImpl().getAcronym())) {
        LOGGER.info("Providing external terminology info information.");
        resultList = externalWS.getTerminologyInfos(terminology_id);
      } else {
        LOGGER.info("Providing internal terminology info information.");
        resultList = GFBioTSInstance.terminologyInfos(terminology_id);
      }
      if (resultList.get(0).isEmpty()) {
        if (!isAvailable(terminology_id)) {
          return createError(format, formatter, 404);
        }
      }
      res = formatter.formatResult(resultList);
    } catch (Exception e) {
      Response r = handleException(e, format, formatter);
      return r;
    }
    return Response.ok(res).header(HttpHeaders.CONTENT_TYPE, getFormat(format)).build();
  }

  @GET
  @Produces(MediaType.APPLICATION_OCTET_STREAM)
  @Path("/{terminology_id}/download")
  public Response downloadTerminology(@Context final UriInfo uriInfo,
      @PathParam("terminology_id") String terminology_id) throws IOException {

    final String calling_uri = uriInfo.getRequestUri().toString();
    final int truncIndex = calling_uri.indexOf("/terminologies");
    Formatter formatter =
        new Formatter("json", StandardVariables.getWsprefix() + calling_uri.substring(truncIndex));

    // get list of all directories in this folder
    String[] files = new File(cfgSvc.getOntoDir() + terminology_id.toLowerCase()).list();
    Arrays.sort(files);
    // select the last one, i.e., the most current one
    String date = files[files.length - 1];

    if (date == null)
      return createError("json", formatter, 400);

    File file = new File(cfgSvc.getOntoDir() + terminology_id.toLowerCase() + "/" + date + "/"
        + terminology_id.toLowerCase() + ".owl");

    LOGGER.info("requested to download " + file.getAbsolutePath());

    return Response.status(200).entity(file)
        .header("Content-Disposition", "attachment; filename=" + file.getName()).build();
  }

  /**
   * Implements the terminology metrics endpoint. Calls the GFBioTSInstance (for internal) or the
   * externalWS (for external) corresponding method then formats the results in the format passed as
   * parameter (default json).
   * 
   * @param uriInfo
   * @param terminology_id
   * @param format
   * @return
   * @throws ServletException
   * @throws IOException
   */
  @GET
  @Path("/{terminology_id}/metrics")
  public Response getMetrics(@Context final UriInfo uriInfo,
      @PathParam("terminology_id") String terminology_id,
      @QueryParam("format") @DefaultValue("json") String format) throws IOException {
    final String calling_uri = uriInfo.getRequestUri().toString();
    final int truncIndex = calling_uri.indexOf("/terminologies");
    Formatter formatter =
        new Formatter(format, StandardVariables.getWsprefix() + calling_uri.substring(truncIndex));
    List<JsonObject> resultList;
    String res;
    try {
      if (this.externalWS.contains(terminology_id)
          || terminology_id.contains(new DWBWSserviceImpl().getAcronym())) {
        LOGGER.info("Providing external metrics information.");
        resultList = externalWS.getMetrics(terminology_id);
      } else {
        LOGGER.info("Providing internal metrics information.");
        resultList = GFBioTSInstance.metrics(terminology_id);
      }
      if (resultList.get(0).isEmpty()) {
        if (!isAvailable(terminology_id)) {
          return createError(format, formatter, 404);
        }
      }
      res = formatter.formatResult(resultList);
    } catch (Exception e) {
      Response r = handleException(e, format, formatter);
      return r;
    }
    return Response.ok(res).header(HttpHeaders.CONTENT_TYPE, getFormat(format)).build();
  }

  /**
   * Implements the terminology metadata endpoint. Calls the GFBioTSInstance (for internal) or the
   * externalWS (for external) corresponding method then formats the results in the format passed as
   * parameter (default json).
   * 
   * @param uriInfo
   * @param terminology_id
   * @param format
   * @return
   * @throws ServletException
   * @throws IOException
   */
  @GET
  @Path("/{terminology_id}/metadata")
  public Response getMetadata(@Context final UriInfo uriInfo,
      @PathParam("terminology_id") String terminology_id,
      @QueryParam("format") @DefaultValue("json") String format) throws IOException {
    final String calling_uri = uriInfo.getRequestUri().toString();
    final int truncIndex = calling_uri.indexOf("/terminologies");
    Formatter formatter =
        new Formatter(format, StandardVariables.getWsprefix() + calling_uri.substring(truncIndex));
    List<JsonObject> resultList;
    String res;
    try {
      if (this.externalWS.contains(terminology_id)
          || terminology_id.contains(new DWBWSserviceImpl().getAcronym())) {
        LOGGER.info("Providing external metadata information.");
        resultList = externalWS.getMetadata(terminology_id);
      } else {
        LOGGER.info("Providing internal metadata information.");
        resultList = GFBioTSInstance.metadata(terminology_id);
      }
      if (resultList.get(0).isEmpty()) {
        if (!isAvailable(terminology_id)) {
          return createError(format, formatter, 404);
        }
      }
      res = formatter.formatResult(resultList);
    } catch (Exception e) {
      Response r = handleException(e, format, formatter);
      return r;
    }
    return Response.ok(res).header(HttpHeaders.CONTENT_TYPE, getFormat(format)).build();
  }


  /**
   * Implements the terminology archive endpoint. Calls the GFBioTSInstance corresponding method
   * then formats the results in the format passed as parameter (default json).
   * 
   * @param uriInfo
   * @param terminology_id
   * @param format
   * @return
   * @throws IOException
   */
  @GET
  @Path("/{terminology_id}/archive")
  public Response getArchive(@Context final UriInfo uriInfo,
      @PathParam("terminology_id") String terminology_id,
      @QueryParam("format") @DefaultValue("json") String format) throws IOException {

    final String calling_uri = uriInfo.getRequestUri().toString();
    final int truncIndex = calling_uri.indexOf("/terminologies");
    Formatter formatter =
        new Formatter(format, StandardVariables.getWsprefix() + calling_uri.substring(truncIndex));
    JsonObject resultList;
    String res;

    LOGGER.info("Providing archive information.");
    resultList = GFBioTSInstance.archive(terminology_id, 0);
    if (resultList.size() == 0) {
      // if (!isAvailable(terminology_id)) {
      return createError(format, formatter, 500);
      // }
    }
    res = formatter.formatResultV2(resultList);

    return Response.ok(res).header(HttpHeaders.CONTENT_TYPE, getFormat(format)).build();
  }

  @GET
  @Path("/{terminology_id}/archive/{archiveId}")
  public Response getArchivedVersion(@Context final UriInfo uriInfo,
      @PathParam("terminology_id") String terminology_id,
      @QueryParam("format") @DefaultValue("json") String format,
      @PathParam("archiveId") @DefaultValue("0") String archiveId) throws IOException {

    final String calling_uri = uriInfo.getRequestUri().toString();
    final int truncIndex = calling_uri.indexOf("/terminologies");
    Formatter formatter =
        new Formatter(format, StandardVariables.getWsprefix() + calling_uri.substring(truncIndex));
    JsonObject resultList;
    String res;

    LOGGER.info("Providing archive information.");
    resultList = GFBioTSInstance.archive(terminology_id, Integer.valueOf(archiveId));
    if (resultList.size() == 0) {
      // if (!isAvailable(terminology_id)) {
      return createError(format, formatter, 500);
      // }
    }
    res = formatter.formatResultV2(resultList);

    return Response.ok(res).header(HttpHeaders.CONTENT_TYPE, getFormat(format)).build();
  }

  @GET
  @Path("/{terminology_id}/latest")
  public Response getLatestTerminologyVersion(@Context final UriInfo uriInfo,
      @PathParam("terminology_id") String terminology_id,
      @QueryParam("format") @DefaultValue("json") String format) throws IOException {

    final String calling_uri = uriInfo.getRequestUri().toString();
    final int truncIndex = calling_uri.indexOf("/terminologies");
    Formatter formatter =
        new Formatter(format, StandardVariables.getWsprefix() + calling_uri.substring(truncIndex));

    LOGGER.info("Providing latest terminology version/metadata.");

    JsonObject resultObject = GFBioTSInstance.latestTerminologyVersion(terminology_id);
    if (resultObject == null || resultObject.size() == 0)
      return createError(format, formatter, 504);

    return Response.ok(formatter.formatResultV2(resultObject))
        .header(HttpHeaders.CONTENT_TYPE, getFormat(format)).build();
  }

  @GET
  @Path("/{terminology_id}/changelog")
  public Response getChangelog(@Context final UriInfo uriInfo,
      @PathParam("terminology_id") String terminology_id,
      @QueryParam("format") @DefaultValue("json") String format,
      @QueryParam("page") @DefaultValue("1") int page,
      @QueryParam("pagesize") @DefaultValue("50") int pagesize) throws IOException {

    final String calling_uri = uriInfo.getRequestUri().toString();
    final int truncIndex = calling_uri.indexOf("/terminologies");
    Formatter formatter =
        new Formatter(format, StandardVariables.getWsprefix() + calling_uri.substring(truncIndex));

    LOGGER.info("Providing changelog information.");
    JsonObject resultList = GFBioTSInstance.changelog(terminology_id,
        StandardVariables.getWsprefix() + "/" + uriInfo.getPath(), page, pagesize);
    if (resultList == null || resultList.size() == 0)
      return createError(format, formatter, 504);

    return Response.ok(formatter.formatResultV2(resultList))
        .header(HttpHeaders.CONTENT_TYPE, getFormat(format)).build();
  }

  @GET
  @Produces(MediaType.APPLICATION_OCTET_STREAM)
  @Path("/{terminology_id}/changelog/download")
  public Response downloadChangelog(@Context final UriInfo uriInfo,
      @PathParam("terminology_id") String terminology_id, @QueryParam("date") String date)
      throws IOException {

    // final String calling_uri = uriInfo.getRequestUri().toString();
    // final int truncIndex = calling_uri.indexOf("/terminologies");
    // Formatter formatter =
    // new Formatter("json", StandardVariables.getWsprefix() + calling_uri.substring(truncIndex));

    // if (date == null)
    // return createError("json", formatter, 400);

    if (date == null) {
      // get list of all directories in this folder
      String[] files = new File(cfgSvc.getOntoDir() + terminology_id.toLowerCase()).list();
      Arrays.sort(files);
      // select the last one, i.e., the most current one
      date = files[files.length - 1];
    }

    File file = new File(cfgSvc.getOntoDir() + terminology_id.toLowerCase() + "/" + date
        + "/output_diff_" + terminology_id + ".xml");

    LOGGER.info("requested to download " + file.getAbsolutePath());

    return Response.status(200).entity(file)
        .header("Content-Disposition", "attachment; filename=" + file.getName()).build();
  }

  /**
   * Implements the mappings endpoint. Calls the GFBioTSInstance corresponding method then formats
   * the results in the format passed as parameter (default json).
   * 
   * @param uriInfo
   * @param terminologies_string
   * @param format
   * @return
   * @throws ServletException
   * @throws IOException
   */
  @GET
  @Path("/mappings")
  public Response getMappings(@Context final UriInfo uriInfo,
      @QueryParam("terminologies") @DefaultValue("") final String terminologies_string,
      @QueryParam("format") @DefaultValue("json") String format) throws IOException {
    final String calling_uri = uriInfo.getRequestUri().toString();
    final int truncIndex = calling_uri.indexOf("/terminologies");
    Formatter formatter =
        new Formatter(format, StandardVariables.getWsprefix() + calling_uri.substring(truncIndex));
    List<JsonObject> resultList;
    String res;
    try {
      LOGGER.info("Providing mappings information.");
      resultList = GFBioTSInstance.mappingsNKA(terminologies_string);
      res = formatter.formatResult(resultList);
    } catch (Exception e) {
      Response r = handleException(e, format, formatter);
      return r;
    }
    return Response.ok(res).header(HttpHeaders.CONTENT_TYPE, getFormat(format)).build();
  }

  /**
   * Implements the terminology capabilities endpoint. Calls the GFBioTSInstance (for internal) or
   * the externalWS (for external) corresponding method then formats the results in the format
   * passed as parameter (default json).
   * 
   * @param uriInfo
   * @param terminology_id
   * @param format
   * @return
   * @throws ServletException
   * @throws IOException
   */
  @GET
  @Path("/{terminology_id}/capabilities")
  public Response getCapabilities(@Context final UriInfo uriInfo,
      @PathParam("terminology_id") String terminology_id,
      @QueryParam("format") @DefaultValue("json") String format) throws IOException {
    final String calling_uri = uriInfo.getRequestUri().toString();
    final int truncIndex = calling_uri.indexOf("/terminologies");
    Formatter formatter =
        new Formatter(format, StandardVariables.getWsprefix() + calling_uri.substring(truncIndex));
    List<JsonObject> resultList;
    String res;
    try {
      if (this.externalWS.contains(terminology_id)
          || terminology_id.contains(new DWBWSserviceImpl().getAcronym())) {
        LOGGER.info("Providing external capabilities information.");
        resultList = externalWS.getCapabilities(terminology_id);
      } else {
        LOGGER.info("Providing internal capabilities information.");
        resultList = GFBioTSInstance.capabilities(terminology_id).create();
      }
      if (resultList.get(0).isEmpty()) {
        if (!isAvailable(terminology_id)) {
          return createError(format, formatter, 404);
        }
      }
      res = formatter.formatResult(resultList);
    } catch (Exception e) {
      Response r = handleException(e, format, formatter);
      return r;
    }
    return Response.ok(res).header(HttpHeaders.CONTENT_TYPE, getFormat(format)).build();
  }

  /**
   * Implements the allterms endpoint. Calls the GFBioTSInstance corresponding method then formats
   * the results in the format passed as parameter (default json).
   * 
   * @param uriInfo
   * @param terminology_id
   * @param format
   * @return
   * @throws ServletException
   * @throws IOException
   */
  @GET
  @Path("/{terminology_id}/allterms")
  public Response getAllTerms(@Context final UriInfo uriInfo,
      @PathParam("terminology_id") String terminology_id,
      @QueryParam("format") @DefaultValue("json") String format) throws IOException {
    final String calling_uri = uriInfo.getRequestUri().toString();
    final int truncIndex = calling_uri.indexOf("/terminologies");
    Formatter formatter =
        new Formatter(format, StandardVariables.getWsprefix() + calling_uri.substring(truncIndex));
    List<JsonObject> resultList;
    String res;
    try {
      if (this.externalWS.contains(terminology_id)
          || terminology_id.contains(new DWBWSserviceImpl().getAcronym())) {
        LOGGER.info("Providing external allterms information.");
        resultList = externalWS.getAllTerms(terminology_id);
      } else {
        LOGGER.info("Providing internal allterms information.");
        resultList = GFBioTSInstance.allTerms(terminology_id);
      }
      if (resultList.isEmpty()) {
        if (!isAvailable(terminology_id)) {
          return createError(format, formatter, 404);
        }
      }
      res = formatter.formatResult(resultList);
    } catch (Exception e) {
      Response r = handleException(e, format, formatter);
      return r;
    }
    return Response.ok(res).header(HttpHeaders.CONTENT_TYPE, getFormat(format)).build();
  }

  /**
   * Implements the term infos endpoint. Calls the GFBioTSInstance (for internal) or the externalWS
   * (for external) corresponding method then formats the results in the format passed as parameter
   * (default json).
   * 
   * @param uriInfo
   * @param terminology_id
   * @param term_uri
   * @param term_id
   * @param format
   * @param output
   * @return
   * @throws ServletException
   * @throws IOException
   */
  @GET
  @Path("/{terminology_id}/term")
  public Response getTermInfos(@Context UriInfo uriInfo,
      @PathParam("terminology_id") final String terminology_id,
      @QueryParam("uri") @DefaultValue("") String term_uri,
      @QueryParam("externalID") @DefaultValue("") String term_id,
      @QueryParam("format") @DefaultValue("json") String format,
      @QueryParam("output") @DefaultValue("combined") String output) throws IOException {
    final String calling_uri = uriInfo.getRequestUri().toString();
    final int truncIndex = calling_uri.indexOf("/terminologies");
    Formatter formatter =
        new Formatter(format, StandardVariables.getWsprefix() + calling_uri.substring(truncIndex));
    List<JsonObject> resultList;
    String res;
    // try{
    if (output.equals("processed") || output.equals("combined") || output.equals("original")) {
      String termIdent = term_id.equals("") ? term_uri : term_id;
      if (termIdent.equals("")) {
        return createError(format, formatter, 400);
      }
      if (this.externalWS.contains(terminology_id)
          || terminology_id.contains(new DWBWSserviceImpl().getAcronym())) {
        LOGGER.info("Providing external term information for " + termIdent);
        resultList = externalWS.getTermInfos(terminology_id, termIdent, output);
      } else {
        LOGGER.info("Providing internal term information.");
        resultList = (GFBioTSInstance.term(terminology_id, termIdent, output, true));
      }
      if (resultList.size() > 0) {
        if (resultList.get(0).isEmpty()) {
          if (!isAvailable(terminology_id)) {
            return createError(format, formatter, 404);
          }
        }
      }
      res = formatter.formatResult(resultList);
    } else {
      LOGGER.info("Wrong output specified.");
      resultList = new ArrayList<JsonObject>();
      resultList
          .add(createWarning("The specified output format is not " + "available. Please choose '"
              + TERMSERVICETYPES.combined.name() + "', " + "'" + TERMSERVICETYPES.processed.name()
              + "' or " + "'" + TERMSERVICETYPES.original.name() + "'."));
      res = formatter.formatResult(resultList);
    }
    // }catch(Exception e) {
    // Response r = handleException(e, format, formatter);
    // return r;
    // }
    return Response.ok(res).header(HttpHeaders.CONTENT_TYPE, getFormat(format)).build();
  }

  /**
   * Implements the suggest endpoint. Calls the GFBioTSInstance corresponding method then formats
   * the results in the format passed as parameter (default json).
   * 
   * @param uriInfo
   * @param search_string
   * @param terminologies_string
   * @param limit_str
   * @return
   */
  @GET
  @Path("/suggest")
  public Response suggest(@Context final UriInfo uriInfo, @QueryParam("query") String search_string,
      @QueryParam("terminologies") @DefaultValue("") final String terminologies_string,
      @QueryParam("limit") @DefaultValue("15") final String limit_str) {
    int limit;
    try {
      limit = Integer.parseInt(limit_str);
    } catch (NumberFormatException e) {
      limit = 15;
    }
    List<JsonObject> resultList = new ArrayList<JsonObject>();
    search_string = search_string.trim();
    if (search_string.length() <= 3) {
      resultList.add(createWarning(
          "The service does not return results for" + " queries shorter than 4 charaters."));

    } else {
      int lastSpace = search_string.lastIndexOf(" ");
      if (search_string.length() - lastSpace - 1 <= 3) {
        resultList.add(createWarning("The service does not return results for"
            + " wildcard queries with less than 4 leading characters"));
      } else {
        resultList = GFBioTSInstance.suggestSearch(search_string, terminologies_string, limit);
      }
    }
    String calling_uri = uriInfo.getRequestUri().toString();
    int trunc_index = calling_uri.indexOf("/terminologies");
    Formatter formatter = new Formatter("json",
        "http://terminologies.gfbio.org/api/" + calling_uri.substring(trunc_index));
    final String res = formatter.formatResult(resultList);
    return Response.ok(res).header(HttpHeaders.CONTENT_TYPE, getFormat("json")).build();
  }

  /**
   * Implements the search endpoint. Calls the searchService corresponding method then formats the
   * results in the format passed as parameter (default json).
   * 
   * @param uriInfo
   * @param search_string
   * @param match_type
   * @param terminologies_string
   * @param first_hit
   * @param format
   * @param internal_only
   * @param filter
   * @return
   * @throws ServletException
   * @throws IOException
   * @throws GFBioBadRequestException
   * @throws GFBioServiceUnavailableException
   */
  @GET
  @Path("/search")
  public Response search(@Context final UriInfo uriInfo, @QueryParam("query") String search_string,
      @QueryParam("match_type") @DefaultValue("exact") final String match_type,
      @QueryParam("terminologies") @DefaultValue("") final String terminologies_string,
      @QueryParam("first_hit") @DefaultValue("false") final String first_hit,
      @QueryParam("format") @DefaultValue("json") String format,
      @QueryParam("internal_only") @DefaultValue("false") final String internal_only,
      @QueryParam("filter") @DefaultValue("") final String filter)
      throws IOException, GFBioBadRequestException, GFBioServiceUnavailableException {
    LOGGER.info(" should process query " + search_string);
    String noiseWordFilter = search_string;
    noiseWordFilter = noiseWordFilter.replaceAll("[^A-Za-z0-9]", "");
    List<JsonObject> resultList = new ArrayList<JsonObject>();
    if (!noiseWordFilter.equals("")) {
      search_string = search_string.replace("\"", "");
      search_string = search_string.replace("'", "");
      resultList = searchService.createAndProcessSearchTasks(search_string.trim(), match_type,
          terminologies_string, first_hit, internal_only, filter);
      LOGGER.info("SPARQL Query processed " + search_string);
    } else {
      resultList.add(createWarning("Search query consists of noise words only: " + search_string));
    }
    if (!format.matches("json|xml|jsonld")) {
      format = "json";
    }
    final String calling_uri = uriInfo.getRequestUri().toString();
    final int truncIndex = calling_uri.indexOf("/terminologies");
    Formatter formatter =
        new Formatter(format, StandardVariables.getWsprefix() + calling_uri.substring(truncIndex));
    final String res = formatter.formatResult(resultList);
    LOGGER.info(" results formatted ... finish ");
    return Response.ok(res).header(HttpHeaders.CONTENT_TYPE, GFBioTermWS.getFormat(format)).build();
  }

  /**
   * Implements the narrower endpoint. Calls the GFBioTSInstance (for internal) or the externalWS
   * (for external) corresponding method then formats the results in the format passed as parameter
   * (default json).
   * 
   * @param uriInfo
   * @param terminology_id
   * @param term_uri
   * @param term_id
   * @param format
   * @return
   * @throws ServletException
   * @throws IOException
   */
  @GET
  @Path("/{terminology_id}/narrower")
  public Response getNarrower(@Context final UriInfo uriInfo,
      @PathParam("terminology_id") String terminology_id,
      @QueryParam("uri") @DefaultValue("") String term_uri,
      @QueryParam("externalID") @DefaultValue("") String term_id,
      @QueryParam("format") @DefaultValue("json") String format) throws IOException {
    final String calling_uri = uriInfo.getRequestUri().toString();
    final int truncIndex = calling_uri.indexOf("/terminologies");
    Formatter formatter =
        new Formatter(format, StandardVariables.getWsprefix() + calling_uri.substring(truncIndex));
    List<JsonObject> resultList;
    String res;
    try {
      String termIdent = term_id.equals("") ? term_uri : term_id;
      if (termIdent.equals("")) {
        return createError(format, formatter, 400);
      }
      if (this.externalWS.contains(terminology_id)
          || terminology_id.contains(new DWBWSserviceImpl().getAcronym())) {
        LOGGER.info("Providing external narrower information.");
        resultList = externalWS.getNarrower(terminology_id, termIdent);
      } else {
        LOGGER.info("Providing internal narrower information.");
        resultList = GFBioTSInstance.narrower(terminology_id, termIdent);
      }
      if (resultList.isEmpty()) {
        if (!isAvailable(terminology_id)) {
          return createError(format, formatter, 404);
        }
      }
      res = formatter.formatResult(resultList);
    } catch (Exception e) {
      Response r = handleException(e, format, formatter);
      return r;
    }
    return Response.ok(res).header(HttpHeaders.CONTENT_TYPE, getFormat(format)).build();
  }

  /**
   * Implements the synonyms endpoint. Calls the GFBioTSInstance (for internal) or the externalWS
   * (for external) corresponding method then formats the results in the format passed as parameter
   * (default json).
   * 
   * @param uriInfo
   * @param terminology_id
   * @param term_uri
   * @param term_id
   * @param format
   * @return
   * @throws ServletException
   * @throws IOException
   */
  @GET
  @Path("/{terminology_id}/synonyms")
  public Response getSynonym(@Context final UriInfo uriInfo,
      @PathParam("terminology_id") String terminology_id,
      @QueryParam("uri") @DefaultValue("") String term_uri,
      @QueryParam("externalID") @DefaultValue("") String term_id,
      @QueryParam("format") @DefaultValue("json") String format) throws IOException {
    final String calling_uri = uriInfo.getRequestUri().toString();
    final int truncIndex = calling_uri.indexOf("/terminologies");
    Formatter formatter =
        new Formatter(format, StandardVariables.getWsprefix() + calling_uri.substring(truncIndex));
    List<JsonObject> resultList;
    String res;
    try {
      String termIdent = term_id.equals("") ? term_uri : term_id;
      if (termIdent.equals("")) {
        return createError(format, formatter, 400);
      }
      if (this.externalWS.contains(terminology_id)
          || terminology_id.contains(new DWBWSserviceImpl().getAcronym())) {
        LOGGER.info("Providing external synonym information.");
        resultList = externalWS.getSynonyms(terminology_id, termIdent);
      } else {
        LOGGER.info("Providing internal synonym information.");
        resultList = GFBioTSInstance.synonym(terminology_id, termIdent);
      }
      if (resultList.isEmpty()) {
        if (!isAvailable(terminology_id)) {
          return createError(format, formatter, 404);
        }
      }
      res = formatter.formatResult(resultList);
    } catch (Exception e) {
      Response r = handleException(e, format, formatter);
      return r;
    }
    return Response.ok(res).header(HttpHeaders.CONTENT_TYPE, getFormat(format)).build();
  }

  /**
   * Implements the broader endpoint. Calls the GFBioTSInstance (for internal) or the externalWS
   * (for external) corresponding method then formats the results in the format passed as parameter
   * (default json).
   * 
   * @param uriInfo
   * @param terminology_id
   * @param term_uri
   * @param term_id
   * @param format
   * @return
   * @throws ServletException
   * @throws IOException
   */
  @GET
  @Path("/{terminology_id}/broader")
  public Response getBroader(@Context final UriInfo uriInfo,
      @PathParam("terminology_id") String terminology_id,
      @QueryParam("uri") @DefaultValue("") String term_uri,
      @QueryParam("externalID") @DefaultValue("") String term_id,
      @QueryParam("format") @DefaultValue("json") String format) throws IOException {
    final String calling_uri = uriInfo.getRequestUri().toString();
    final int truncIndex = calling_uri.indexOf("/terminologies");
    Formatter formatter =
        new Formatter(format, StandardVariables.getWsprefix() + calling_uri.substring(truncIndex));
    List<JsonObject> resultList;
    String res;
    try {
      String termIdent = term_id.equals("") ? term_uri : term_id;
      if (termIdent.equals("")) {
        return createError(format, formatter, 400);
      }
      if (this.externalWS.contains(terminology_id)
          || terminology_id.contains(new DWBWSserviceImpl().getAcronym())) {
        LOGGER.info("Providing external broader information.");
        resultList = externalWS.getBroader(terminology_id, termIdent);
      } else {
        LOGGER.info("Providing internal broader information.");
        resultList = GFBioTSInstance.broader(terminology_id, termIdent);
      }
      if (resultList.isEmpty()) {
        if (!isAvailable(terminology_id)) {
          return createError(format, formatter, 404);
        }
      }
      res = formatter.formatResult(resultList);
    } catch (Exception e) {
      Response r = handleException(e, format, formatter);
      return r;
    }
    return Response.ok(res).header(HttpHeaders.CONTENT_TYPE, getFormat(format)).build();
  }

  /**
   * Implements the hierarchy endpoint. Calls the GFBioTSInstance (for internal) or the externalWS
   * (for external) corresponding method then formats the results in the format passed as parameter
   * (default json).
   * 
   * @param uriInfo
   * @param terminology_id
   * @param term_uri
   * @param term_id
   * @param format
   * @return
   * @throws ServletException
   * @throws IOException
   */
  @GET
  @Path("/{terminology_id}/hierarchy")
  public Response getHierarchy(@Context final UriInfo uriInfo,
      @PathParam("terminology_id") String terminology_id,
      @QueryParam("uri") @DefaultValue("") String term_uri,
      @QueryParam("externalID") @DefaultValue("") String term_id,
      @QueryParam("format") @DefaultValue("json") String format) throws IOException {
    final String calling_uri = uriInfo.getRequestUri().toString();
    final int truncIndex = calling_uri.indexOf("/terminologies");
    Formatter formatter =
        new Formatter(format, StandardVariables.getWsprefix() + calling_uri.substring(truncIndex));
    List<JsonObject> resultList;
    String res;
    try {
      String termIdent = term_id.equals("") ? term_uri : term_id;
      if (termIdent.equals("")) {
        return createError(format, formatter, 400);
      }
      if (this.externalWS.contains(terminology_id)
          || terminology_id.contains(new DWBWSserviceImpl().getAcronym())) {
        LOGGER.info("Providing external hierarchy information.");
        resultList = externalWS.getHierarchy(terminology_id, termIdent);
      } else {
        LOGGER.info("Providing internal hierarchy information.");
        resultList = GFBioTSInstance.hierarchy(terminology_id, termIdent);
      }
      if (resultList.isEmpty()) {
        if (!isAvailable(terminology_id)) {
          return createError(format, formatter, 404);
        }
      }
      res = formatter.formatResult(resultList);
    } catch (Exception e) {
      Response r = handleException(e, format, formatter);
      return r;
    }
    return Response.ok(res).header(HttpHeaders.CONTENT_TYPE, getFormat(format)).build();
  }

  /**
   * Implements the allnarrower endpoint. Calls the GFBioTSInstance (for internal) or the externalWS
   * (for external) corresponding method then formats the results in the format passed as parameter
   * (default json).
   * 
   * @param uriInfo
   * @param terminology_id
   * @param term_uri
   * @param term_id
   * @param format
   * @return
   * @throws ServletException
   * @throws IOException
   */
  @GET
  @Path("/{terminology_id}/allnarrower")
  public Response getAllNarrower(@Context final UriInfo uriInfo,
      @PathParam("terminology_id") String terminology_id,
      @QueryParam("uri") @DefaultValue("") String term_uri,
      @QueryParam("externalID") @DefaultValue("") String term_id,
      @QueryParam("format") @DefaultValue("json") String format) throws IOException {
    final String calling_uri = uriInfo.getRequestUri().toString();
    final int truncIndex = calling_uri.indexOf("/terminologies");
    Formatter formatter =
        new Formatter(format, StandardVariables.getWsprefix() + calling_uri.substring(truncIndex));
    List<JsonObject> resultList;
    String res;
    try {
      String termIdent = term_id.equals("") ? term_uri : term_id;
      if (termIdent.equals("")) {
        return createError(format, formatter, 400);
      }
      if (this.externalWS.contains(terminology_id)
          || terminology_id.contains(new DWBWSserviceImpl().getAcronym())) {
        LOGGER.info("Providing external allnarrower information.");
        resultList = externalWS.getAllNarrower(terminology_id, termIdent);
      } else {
        LOGGER.info("Providing internal allnarrower information.");
        resultList = GFBioTSInstance.allNarrower(terminology_id, termIdent);
      }
      if (resultList.isEmpty()) {
        if (!isAvailable(terminology_id)) {
          return createError(format, formatter, 404);
        }
      }
      res = formatter.formatResult(resultList);
    } catch (Exception e) {
      Response r = handleException(e, format, formatter);
      return r;
    }
    return Response.ok(res).header(HttpHeaders.CONTENT_TYPE, getFormat(format)).build();
  }

  /**
   * Implements the allbroader endpoint. Calls the GFBioTSInstance (for internal) or the externalWS
   * (for external) corresponding method then formats the results in the format passed as parameter
   * (default json).
   * 
   * @param uriInfo
   * @param terminology_id
   * @param term_uri
   * @param term_id
   * @param format
   * @return
   * @throws ServletException
   * @throws IOException
   */
  @GET
  @Path("/{terminology_id}/allbroader")
  public Response getAllBroader(@Context final UriInfo uriInfo,
      @PathParam("terminology_id") String terminology_id,
      @QueryParam("uri") @DefaultValue("") String term_uri,
      @QueryParam("externalID") @DefaultValue("") String term_id,
      @QueryParam("format") @DefaultValue("json") String format) throws IOException {
    final String calling_uri = uriInfo.getRequestUri().toString();
    final int truncIndex = calling_uri.indexOf("/terminologies");
    Formatter formatter =
        new Formatter(format, StandardVariables.getWsprefix() + calling_uri.substring(truncIndex));
    List<JsonObject> resultList;
    String res;
    try {
      String termIdent = term_id.equals("") ? term_uri : term_id;
      if (termIdent.equals("")) {
        return createError(format, formatter, 400);
      }
      if (this.externalWS.contains(terminology_id)
          || terminology_id.contains(new DWBWSserviceImpl().getAcronym())) {
        LOGGER.info("Providing external allbroader information.");
        resultList = externalWS.getAllBroader(terminology_id, termIdent);
      } else {
        LOGGER.info("Providing internal allbroader information.");
        resultList = GFBioTSInstance.allBroader(terminology_id, termIdent);
      }
      if (resultList.isEmpty()) {
        if (!isAvailable(terminology_id)) {
          return createError(format, formatter, 404);
        }
      }
      res = formatter.formatResult(resultList);
    } catch (Exception e) {
      Response r = handleException(e, format, formatter);
      return r;
    }
    return Response.ok(res).header(HttpHeaders.CONTENT_TYPE, getFormat(format)).build();
  }

  /**
   * @param format: the string passed by the user as a queryParam. (xml,json,rdf,csv)
   * @return returns the corresponding MediaType for correct result type
   */
  private static String getFormat(String format) { // FIXME move to the formatter class
    String outputformat = MediaType.APPLICATION_JSON;
    if (format != null) {
      format = format.toLowerCase();
      switch (format) {
        case "xml":
          outputformat = MediaType.APPLICATION_XML;
          break;
        case "json":
          outputformat = MediaType.APPLICATION_JSON;
          break;
        case "rdf":
          outputformat = MediaType.APPLICATION_XML;
          break;
        case "csv":
          outputformat = MediaType.TEXT_PLAIN;
          break;
      }
    }
    return outputformat + ";charset=UTF-8";
  }

  private Response handleException(Exception e, String format, Formatter formatter) {
    logException(e);
    Response r;
    if (e.getMessage() != null && e.getMessage().toLowerCase().contains("virtuoso")) {
      r = createError(format, formatter, 503);
    } else {
      r = createError(format, formatter, 520);
    }
    return r;
  }

  private JsonObject createWarning(String message) {
    return Json.createObjectBuilder().add("warning", message).build();
  }

  private Response createError(String format, Formatter formatter, int code) {
    JsonObjectBuilder j = Json.createObjectBuilder();
    if (code == 503) {
      j.add("error", "503 Service Unavailable");
    } else if (code == 520) {
      j.add("error", "520 Error Unknown: An unknown error occured. Please check your input.");
    } else if (code == 404) {
      j.add("error", "404 Not Found: The terminology you provided was not found.");
    } else if (code == 400) {
      j.add("error", "400 Bad Request: Parameter is missing.");
    } else if (code == 500) {
      j.add("error", "501 Server Error: The archive for this terminology is not yet available.");
    } else if (code == 502) {
      j.add("error", "502 Server Error: The changelog for this terminology is not yet available.");
    } else if (code == 504) {
      j.add("error",
          "504 Server Error: The changelog is not available yet or there are no more terms to show.");
    }
    ArrayList<JsonObject> a = new ArrayList<JsonObject>();
    a.add(j.build());
    String res = formatter.formatResult(a);
    return Response.status(code).entity(res).header(HttpHeaders.CONTENT_TYPE, getFormat(format))
        .build();
  }

  private void logException(Exception e) {
    Writer writer = new StringWriter();
    PrintWriter printWriter = new PrintWriter(writer);
    e.printStackTrace(printWriter);
    LOGGER.error("Exception caught: " + writer.toString() + "printing generic error to user.");
  }

  private boolean isAvailable(String terminologyId) {
    String sparql =
        "select * from <" + StandardVariables.getUriprefix() + StandardVariables.getMetadataGraph()
            + "> {?s omv:acronym ?o . ?o bif:contains '" + terminologyId + "'}";
    VirtGraph set = VirtGraphSingleton.getInstance().getVirtGraph();
    VirtuosoQueryExecution vqe = new VirtuosoQueryExecution(sparql, set);
    ResultSet rs = vqe.execSelect();
    if (rs.hasNext() || this.externalWS.contains(terminologyId)
        || terminologyId.contains(new DWBWSserviceImpl().getAcronym())) {
      vqe.close();
      return true;
    }
    vqe.close();
    return false;
  }
}
