package org.gfbio.terminologies.restws;

import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import javax.json.JsonObject;

import org.apache.log4j.Logger;

public class WSTasksHandling {

	private static final Logger LOGGER = Logger.getLogger(WSTasksHandling.class);

	private static ArrayBlockingQueue<Runnable> taskQueue;

	private static ThreadPoolExecutor fixedJobThreadPool;
	
	private static ScheduledExecutorService webserviceWatcher;
	
	//constants
	private final static int THREADQUEUESIZE = 101;

	private final static int COREPOOLSIZE = 4;

	private final static int MAXIMUMPOOLSIZE = 8;

	private final static long KEEPALIVETIME = 42;
	
	//private final static ConcurrentHashMap<Future, List<JsonObject>> requestResultPool;
	
	private static final WSTasksHandling WSTASKSINSTANCE = new WSTasksHandling();

	private WSTasksHandling() {
		taskQueue = new ArrayBlockingQueue<Runnable>(THREADQUEUESIZE);
		fixedJobThreadPool = new ThreadPoolExecutor(COREPOOLSIZE, MAXIMUMPOOLSIZE, KEEPALIVETIME, TimeUnit.SECONDS, taskQueue);
		webserviceWatcher = Executors.newScheduledThreadPool(1);
		LOGGER.info("webservice watcher is prepared");
		LOGGER.info(" threadpool created and ready" + fixedJobThreadPool.getThreadFactory().toString());
		
	}

	/**
	 * submit and EXEcute ! a task as a thread
	 * @param task
	 * @return
	 */
	public Future<List<JsonObject>> submitTask(final Callable <List<JsonObject>> task) { //TODO this can be moved out as a generic

		final Future<List<JsonObject>> future = fixedJobThreadPool.submit(task);
		LOGGER.info("task added to q " + task.toString() +" now parallel active running tasks = " +fixedJobThreadPool.getActiveCount());
		
		return future;
	}

	/**
	 * use this to feed the scheduled pool with  one single task at a fixed iteration
	 * this task should remove webservice with no lifesign or readd a webservice
	 * more we can only do with OSGI or poorly with a classpath update at the runtime to load new jars ...
	 * @param watchterTask
	 */
	public void submitScheduledtask(Runnable watchterTask) {
		
		webserviceWatcher.scheduleAtFixedRate(watchterTask, 0, 15, TimeUnit.MINUTES);
		
	}
	
	/**
	 * @return the wstasksInstance
	 */
	public static final WSTasksHandling getWstasksInstance() {
		return WSTASKSINSTANCE;
	}

	/**
	 * use this method as a good and clear shutdown of this threadpool
	 * a soft shutdown: all active tasks can be finish
	 */
	public void shutdownWSTasks() {
		webserviceWatcher.shutdown();
		LOGGER.info("webservice watcher is down");
		fixedJobThreadPool.shutdown();
		LOGGER.info("threadpool is down");
	}
}
