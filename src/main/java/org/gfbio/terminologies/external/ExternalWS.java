package org.gfbio.terminologies.external;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import javax.json.JsonObject;
import org.apache.jena.atlas.lib.Pair;
import org.apache.log4j.Logger;
import org.gfbio.colwebservice.CatalogueOfLife;
import org.gfbio.interfaces.WSInterface;
import org.gfbio.pesiwebservice.PESIWSServiceImpl;
import org.gfbio.resultset.GFBioResultSet;
import org.gfbio.terminologies.environment.StandardVariables;
import org.gfbio.terminologies.environment.StandardVariables.TERMSERVICETYPES;
import org.gfbio.terminologies.restws.WSTask;
import org.gfbio.terminologies.restws.WSTasksHandling;
import org.gfbio.wormswebservice.WORMSWSServiceIMPL;
import webservice.DWBWSserviceImpl;

/**
 * This class is a register for external web services like the Catalogue of life, WoRMS, etc. A
 * HashMap is used to store the instances of those services
 */
public class ExternalWS {

  private static final Logger LOGGER = Logger.getLogger(WSTasksHandling.class);

  private HashMap<String, WSInterface> WSList;
  private static ExternalWS instance = null;

  private final static WSTasksHandling wsTasksHandling = WSTasksHandling.getWstasksInstance();;

  public ExternalWS() {

    // Add all external web services here,
    /*
     * FIXME create a lifesign and remover watcher (TimerTask) this means a static scheduled MINI
     * threadpool or Task for the whole servlet runtime liek the ITIS case of 20. november 2014
     */
    // with OSGi we can here code a live admin function for insert and remove
    // webservices
    WSList = new HashMap<String, WSInterface>();

    WSInterface col = new CatalogueOfLife();
    WSList.put(col.getAcronym(), col);

    WSInterface worms = new WORMSWSServiceIMPL();
    WSList.put(worms.getAcronym(), worms);

    // with release from 02/21 on ITIS is available as internal terminology
    // WSInterface itis = new ITISRestfulWSImpl();
    // WSList.put(itis.getAcronym(), itis);

    WSInterface pesi = new PESIWSServiceImpl();
    WSList.put(pesi.getAcronym(), pesi);

    // WSInterface geonames = new GeonamesWSServiceImpl();
    // WSList.put(geonames.getAcronym(), geonames);

    WSInterface dwb = new DWBWSserviceImpl();
    WSList.put(dwb.getAcronym(), dwb);

    // WSInterface pnu = new PNUWebserviceImpl();
    // WSList.put(pnu.getAcronym(), pnu);

    LOGGER.info("External webservices ready for use");
  }

  public static ExternalWS getInstance() {
    if (instance == null) {
      instance = new ExternalWS();
    }
    return instance;
  }

  public boolean contains(String acronym) {
    return WSList.containsKey(acronym);
  }

  public HashMap<String, WSInterface> getWsList() {
    return WSList;
  }

  /**
   * FIXME write me
   * 
   * @param terminologyID
   * @param termUri
   * @return
   */
  public List<JsonObject> getAllBroader(final String terminologyID,
      final String term_uriORexternalID) {
    WSInterface ws;
    if (terminologyID.contains(StandardVariables.getDtntaxonlistswebserviceacronym())) {
      String[] att = terminologyID.split("-");
      ws = new DWBWSserviceImpl(att[1], att[2]);
    } else {
      ws = WSList.get(terminologyID);
    }
    return ws.getAllBroader(term_uriORexternalID).create();
  }

  public List<JsonObject> getHierarchy(final String terminologyID,
      final String term_uriORexternalID) {
    WSInterface ws;
    if (terminologyID.contains(StandardVariables.getDtntaxonlistswebserviceacronym())) {
      String[] att = terminologyID.split("-");
      ws = new DWBWSserviceImpl(att[1], att[2]);
    } else {
      ws = WSList.get(terminologyID);
    }
    return ws.getHierarchy(term_uriORexternalID).create();
  }

  @SuppressWarnings({"rawtypes", "unchecked"})
  public List<JsonObject> getBroader(final String terminologyID,
      final String term_uriORexternalID) {

    GFBioResultSet rs = new GFBioResultSet("internal");
    rs.addWarning("The requested service is not available for the given terminology");
    return rs.create();
  }

  @SuppressWarnings({"rawtypes", "unchecked"})
  public List<JsonObject> getNarrower(final String terminologyID,
      final String term_uriORexternalID) {
    GFBioResultSet rs = new GFBioResultSet("internal");
    rs.addWarning("The requested service is not available for the given terminology");
    return rs.create();
  }

  @SuppressWarnings({"rawtypes", "unchecked"})
  public List<JsonObject> getAllNarrower(final String terminologyID,
      final String term_uriORexternalID) {
    GFBioResultSet rs = new GFBioResultSet("internal");
    rs.addWarning("The requested service is not available for the given terminology");
    return rs.create();
  }

  @SuppressWarnings({"rawtypes", "unchecked"})
  public List<JsonObject> getMetrics(final String terminologyID) {
    GFBioResultSet rs = new GFBioResultSet("internal");
    rs.addWarning("The requested service is not available for the given terminology");
    return rs.create();
  }

  @SuppressWarnings({"rawtypes", "unchecked"})
  public List<JsonObject> getMetadata(final String terminologyID) {
    GFBioResultSet rs = new GFBioResultSet("internal");
    rs.addWarning("The requested service is not available for the given terminology");
    return rs.create();
  }

  @SuppressWarnings({"rawtypes", "unchecked"})
  public List<JsonObject> getAllTerms(final String terminologyID) {
    GFBioResultSet rs = new GFBioResultSet("internal");
    rs.addWarning("The requested service is not available for the given terminology");
    return rs.create();
  }

  @SuppressWarnings({"rawtypes", "unchecked"})
  public List<JsonObject> getTermInfos(final String terminologyID,
      final String term_uriORexternalID, final String termServiceType) {
    WSInterface ws;
    if (terminologyID.contains(StandardVariables.getDtntaxonlistswebserviceacronym())) {
      String[] att = terminologyID.split("-");
      ws = new DWBWSserviceImpl(att[1]);
    } else {
      ws = WSList.get(terminologyID);
    }
    // request parameter &output=
    // 'combined' -> default
    if (termServiceType.toLowerCase().equals(TERMSERVICETYPES.original.name())) {
      return ws.getTermInfosOriginal(term_uriORexternalID).create();
    } else if (termServiceType.toLowerCase().equals(TERMSERVICETYPES.processed.name())) {
      return ws.getTermInfosProcessed(term_uriORexternalID).create();
    } else if (termServiceType.toLowerCase().equals(TERMSERVICETYPES.combined.name())) {
      return ws.getTermInfosCombined(term_uriORexternalID).create();
    }
    GFBioResultSet rs = new GFBioResultSet("internal");
    rs.addWarning("The term service type" + termServiceType + "is unknown. Please specify"
        + TERMSERVICETYPES.original.name() + ", " + TERMSERVICETYPES.processed.name() + " or "
        + TERMSERVICETYPES.combined.name());
    return rs.create();
  }

  @SuppressWarnings({"rawtypes", "unchecked"})
  public List<JsonObject> getSynonyms(final String terminologyID,
      final String term_uriORexternalID) {
    if (terminologyID.equals("COL")) {
      GFBioResultSet rs = new GFBioResultSet("internal");
      rs.addWarning("The requested service is not available for the given " + "terminology");
      return rs.create();
    }
    WSInterface ws;
    if (terminologyID.contains(StandardVariables.getDtntaxonlistswebserviceacronym())) {
      String[] att = terminologyID.split("-");
      ws = new DWBWSserviceImpl(att[1], att[2]);
    } else {
      ws = WSList.get(terminologyID);
    }
    return ws.getSynonyms(term_uriORexternalID).create();
  }

  public List<JsonObject> getTerminologyInfos(final String terminologyID) {
    WSInterface ws;
    if (terminologyID.contains(StandardVariables.getDtntaxonlistswebserviceacronym())) {
      String[] att = terminologyID.split("-");
      if (att.length == 2) {
        ws = new DWBWSserviceImpl(att[1]);
      } else {
        ws = new DWBWSserviceImpl();
      }
    } else {
      ws = WSList.get(terminologyID);
    }
    return ws.getMetadata().create();
  }

  public List<JsonObject> getCapabilities(final String terminologyID) {
    WSInterface ws;
    if (terminologyID.contains(StandardVariables.getDtntaxonlistswebserviceacronym())) {
      String[] att = terminologyID.split("-");
      if (att.length == 2) {
        ws = new DWBWSserviceImpl(att[1]);
      } else {
        ws = new DWBWSserviceImpl();
      }
    } else {
      ws = WSList.get(terminologyID);
    }
    return ws.getCapabilities().create();
  }

  /**
   * Searches all registered external terminologies
   * 
   * @param searchString
   * @param matchType
   * @param terminologyList
   * @return
   */
  public List<JsonObject> search(final String searchString, final String matchType,
      final List<String> terminologyList) {
    List<JsonObject> result = Collections.synchronizedList(new ArrayList<JsonObject>());
    List<Future<List<JsonObject>>> externalWSTasks = new ArrayList<>();

    if (terminologyList.isEmpty())
      terminologyList.addAll(WSList.keySet());

    // start the time measurement
    final long starttime = System.nanoTime();
    ArrayList<Pair<String, Long>> time = new ArrayList<>();
    ArrayList<Pair<Future<List<JsonObject>>, String>> dict = new ArrayList<>();

    for (String terminology : terminologyList) {
      WSInterface ws = WSList.get(terminology);

      if (ws != null && ws.supportsMatchType(matchType) && ws.isResponding()) {

        // Thread creations put the threads to the threadpool
        final WSTask wsTask = new WSTask(ws, searchString, matchType);
        final Future<List<JsonObject>> wsTaskLink = wsTasksHandling.submitTask(wsTask);
        externalWSTasks.add(wsTaskLink);
        dict.add(new Pair<Future<List<JsonObject>>, String>(wsTaskLink, terminology));
        LOGGER.info("external webservice task as a thread started " + ws.getAcronym());
      }
    }

    LOGGER.info("external webservice tasks are running totally " + externalWSTasks.size());
    // await the finish of all created threads and

    for (Future<List<JsonObject>> wsTaskLink : externalWSTasks) {

      /*
       * tricky way, this call blockes until THIS thread task has been finished so the longest
       * execution of a single thread task is the whole processtime for all webservice calls!
       */

      try {
        final List<JsonObject> threadResult = wsTaskLink.get();
        // if done this adressed thread gives us his result and we add this to the
        // central resultset
        if (threadResult != null) {
          result.addAll(threadResult);
        } else {
          LOGGER.info("thread returns null ");
        }
        LOGGER.info("a external webservice task is done ");

        // find corresponding terminology and note the time
        String name = null;
        for (Pair<Future<List<JsonObject>>, String> task : dict) {
          if (task.getLeft().equals(wsTaskLink)) {
            name = task.getRight();
          }
        }
        if (name == null)
          LOGGER.info(
              "Something went wrong! Not able to find a correspondence between threads and their returned futures!");
        time.add(new Pair<String, Long>(name, System.nanoTime() - starttime));

      } catch (InterruptedException e) {
        LOGGER.error(e);
        e.printStackTrace();
      } catch (ExecutionException e) {
        LOGGER.error(e);
        e.printStackTrace();
      }
    }
    LOGGER.info("all asynchronous external webservice tasks are done ");

    // print table of thread times
    String leftAlignFormat = "| %-19s | %-9f |";
    LOGGER.info("Table of durations in order to fullfill the request to the external webservice:");
    LOGGER.info("+---------------------+-----------+");
    LOGGER.info("| name of webservice  | time(in s)|");
    LOGGER.info("+---------------------+-----------+");
    for (Pair<String, Long> pair : time) {
      LOGGER.info(String.format(leftAlignFormat, pair.getLeft(), pair.getRight() / 1000000000.0));
    }
    LOGGER.info("+---------------------+-----------+");;
    return result;
  }

  /**
   * use only a single external terminology
   * 
   * @param search_string
   * @param match_type
   * @param terminology
   * @return
   */
  public List<JsonObject> search(final String search_string, final String match_type,
      final String terminology) {
    ArrayList<String> singleTerminology = new ArrayList<>();
    singleTerminology.add(terminology.toUpperCase());
    return search(search_string, match_type, singleTerminology);
  }
}
